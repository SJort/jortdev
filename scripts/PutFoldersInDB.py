import os
import posix
import mariadb
import sys

sourcePath = "/home/jort/this/"
destinationPath = "/home/jort/this/jortdev/images"

# stop if folder does not exist
if not os.path.exists(destinationPath):
    print(f"Folder does not exist: {destinationPath}")
    exit(1)


# connect to db, stop if connection failed
try:
    dbConnection = mariadb.connect(
        user="root",
        password="root",
        host="127.0.0.1",
        port=3306,
        database="websitedb"
    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# retrieve folder names already in database
cursor = dbConnection.cursor()
pathsAlreadyInDb = []
cursor.execute("SELECT path FROM folder;")
for (x,) in cursor:
    pathsAlreadyInDb.append(x)

# iterate all folders, and add them to database if not path not already in there
amountOfFolders = 0
for entry in os.scandir(destinationPath):
    try:
        if isinstance(entry, posix.DirEntry):
            amountOfFolders += 1
            folderName = entry.name
            folderPath = os.path.join(destinationPath, folderName)
            if folderPath not in pathsAlreadyInDb:
                print(f"Adding {folderPath} to DB.")
                cursor.execute("INSERT INTO folder (name, path) VALUES (?, ?);", (folderName, folderPath))
    except Exception as e:
        print(f"Fail while scanning folders: {e}")

print(f"Scanned {amountOfFolders} folders and added {amountOfFolders - len(pathsAlreadyInDb)} of them in the database.")

# actually insert items into database and close connection
dbConnection.commit()
dbConnection.close()
