<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/error-box.php";

$sql = "SELECT * from user;";
$users = [];

global $pdo;
if ($stmt = $pdo->prepare($sql)) {
    if ($stmt->execute()) {
        if ($stmt->rowCount() > 1) {
            while ($row = $stmt->fetch()) {
                array_push($users, $row["username"]);
            }
        }
    }

    unset($stmt);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Users</title>
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="center-horizontal-parent">
<div class="center-child">
    <h2>List of users</h2>
    <table class="table">
        <?php
        foreach ($users as $user) {
            $url = "/user/" . $user;
            echo "<tr><th><a class='link' href='", $url, "'>", $user, "</a></th></tr>";
        }
        ?>
    </table>
</div>
</body>
</html>
