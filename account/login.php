<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/account/login-functions.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/logging.php";

$username = $password = "";
$username_err = $password_err = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = $_POST["username"];
    $password = $_POST["password"];
    echo "Username: " . $username . ", password: " . $password;

    if (!is_username_allowed($username)) {
        $username_err = "Please enter username.";
    }

    if (!does_user_exist($username)) {
        $username_err = "User does not exist.";
    }

    if (empty($username_err) && empty($password_err)) {
        if (login($username, $password)) {
            header("Location: /user/" . $username);
            exit();
        }
        else {
            $password_err = "Error logging in.";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Login</title>
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="center-parent">
<div class="center-child">
    <h2>Login</h2>
    <p>Please fill in your credentials to login.</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
            <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label>Password</label>
            <input type="password" name="password" class="form-control">
            <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Login">
        </div>
        <p>Don't have an account? <a href="/account/register.php">Sign up now</a>.</p>
    </form>
</div>
</body>
</html>
