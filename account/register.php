<?php
try {
// Include config file
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/config.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/account/login-functions.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . "/php/logging.php";

// Define variables and initialize with empty values
    $username = $password = $mail = "";
    $username_err = $password_err = $mail_err = "";

// Processing form data when form is submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $username = $_POST["username"];
        $password = $_POST["password"];
        if (does_user_exist($username)) {
            $username_err = "This username is already taken.";
        }
        else if (!is_username_allowed($username)) {
            $username_err = "Username not allowed.";
        }

        if (empty($username_err) && empty($password_err)) {
            if (register($username, $password, 0)) {
                header("Location: /account/login.php");
            }
            else{
                $password_err = "Error registering user.";
            }
        }
    }
} catch (Exception $e) {
    echo "Exception: ", $e->getMessage(), "\n";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Sign Up</title>
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="center-parent">
<div class="center-child jort-form" style="width:350px">
    <h2>Sign Up</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
            <label>*Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
            <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label>*Password</label>
            <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
            <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($mail_err)) ? 'has-error' : ''; ?>">
            <label>Mail</label>
            <input type="text" name="mail" class="form-control" value="<?php echo $mail; ?>">
            <span class="help-block"><?php echo $mail_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit">
            <input type="reset" class="btn btn-default" value="Reset">
        </div>
        <p>Already have an account? <a href="/account/login.php">Login here</a>.</p>
    </form>
</div>
</body>
</html>