<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/php/config.php";


function session_go(){
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
}

function ip_login(){
    $username = $_SERVER['REMOTE_ADDR'];
    if(!does_user_exist($username)){
        //todo: register with other user type
        register($username, "", 1);
    }
    login($username, "");
    $_SESSION["ip_login"] = true;
}

function is_logged_in(){
    session_go();
    if(!isset($_SESSION["logged_in"])){
        return false;
    }
    if($_SESSION["logged_in"] !== true){
        return false;
    }
    //check if the user actually exists, when database is dropped modified and the session still exists
    //i do this in this function because i check if i am logged in when doing account related things
    if(!does_user_exist($_SESSION["username"])){
        return false;
    }

    return true;
}

function logout(){
    $_SESSION = array();
    session_destroy();
}

/**
 * @param $username String Login.
 * @param $password String Password.
 * @return bool True if now logged in and in session.
 */
function login($username, $password)
{
    $sql = "SELECT * FROM user WHERE username = :username";

    global $pdo;
    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":username", $username, PDO::PARAM_STR);

        if ($stmt->execute()) {
            // Check if username exists, if yes then verify password
            if ($stmt->rowCount() >= 1) {
                if ($row = $stmt->fetch()) {
                    $id = $row["id"];
                    $username = $row["username"];
                    $hashed_password = $row["password"];
                    if (password_verify($password, $hashed_password)) {
                        //put the stuff in the session on successful login
                        session_go();
                        $_SESSION["logged_in"] = true;
                        $_SESSION["user_id"] = $id;
                        $_SESSION["username"] = $username;
                        $_SESSION["user_type"] = $row["type"];

                        unset($stmt);
                        return true;
                    }
                }
            }
        }
    }
    unset($stmt);
    return false;
}

/**
 * @param $username String The username to test.
 * @return bool True if the username is allowed.
 */
function is_username_allowed($username)
{
    //dont allow leading and trailing spaces
    if ($username !== trim($username)) {
        return false;
    }

    //dont allow empty usernames
    if (empty($username)) {
        return false;
    }

    return true;
}

/**
 * Register an user. Check inputs before using this function.
 * @param $username String The username to register.
 * @param $password String The password to register. Gets hashed first.
 * @return bool False if registering failed.
 */
function register($username, $password, $user_type)
{
    if (!is_username_allowed($username)) {
        return false;
    }

    if (does_user_exist($username)) {
        return false;
    }

    $sql = "INSERT INTO user (username, password, type) VALUES (:username, :password, :type)";

    global $pdo;
    if ($stmt = $pdo->prepare($sql)) {

        $password = password_hash($password, PASSWORD_DEFAULT);

        $stmt->bindParam(":username", $username, PDO::PARAM_STR);
        $stmt->bindParam(":password", $password, PDO::PARAM_STR);
        $stmt->bindParam(":type", $user_type, PDO::PARAM_STR);

        if ($stmt->execute()) {
            unset($stmt);
            return true;
        }
    }
    unset($pdo);
    return false;
}

/**
 * @param $username String The username to test.
 * @return bool False if username exists in database or if an error occurred.
 */
function does_user_exist($username)
{
    $sql = "SELECT id FROM user WHERE username = :username";

    global $pdo;
    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":username", $username, PDO::PARAM_STR);

        if ($stmt->execute()) {
            if ($stmt->rowCount() >= 1) {
                unset($stmt);
                return true;
            }
        }
    }
    unset($stmt);
    return false;
}

?>
