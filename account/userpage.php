<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/config.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/account/login-functions.php";

session_go();

$selected_user = "Anonymous";
if (isset($_GET["user"])) {
    $selected_user = $_GET["user"];
}

$user_exists = does_user_exist($selected_user);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>User: <?php echo $selected_user ?></title>
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="center-parent">
<div class="center-child">
    <?php
    if (!$user_exists) {
        echo "<h1 class='display-1'>This user does not exist</h1>";
    } else {
        echo "<h1 class='display-1'>$selected_user</h1>";
    }
    echo "<br><br>";

    echo "<a href='/user/" . $selected_user . "/votes' class='btn btn-success'>Upvoted comments</a><br><br>";
    echo "<a href='/user/" . $selected_user . "/comments' class='btn btn-success'>Placed comments</a><br><br>";
    //if user page is logged in user, show account edit options
    if ($selected_user === $_SESSION["username"]) {
        echo "<a href='/user/" . $selected_user . "/log' class='btn btn-success'>See your log</a><br><br>";
        echo "<a href='/account/set_username.php' class='btn btn-success'>Set username</a><br><br>";
        echo "<a href='/account/reset-password.php' class='btn btn-warning'>Reset Your Password</a><br><br>";
        echo "<a href='/account/logout.php' class='btn btn-danger'>Sign Out of Your Account</a><br><br>";
    }
    ?>
</div>

<script src="/libraries/jquery-3.4.1.min.js"></script>
<script src="/libraries/bootstrap/js/bootstrap.bundle.js"></script>
</body>
</html>
