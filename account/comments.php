<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/error-box.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/comments/vote-functions.php";

$selected_user = "Anonymous";
if (isset($_GET["user"])) {
    $selected_user = $_GET["user"];
}

$user_exists = does_user_exist($selected_user);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comments of <?php echo $selected_user ?></title>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link rel="stylesheet" href="/css/table-style.css">
</head>
<body class="center-horizontal-parent">
<div class="center-child">
    <?php
    if ($user_exists) {
        echo "<h2>Comments of " . $selected_user . "</h2>";
        echo "<table>";

        $sql = "SELECT DATE_FORMAT(comment.creation_time, '%e/%m/%y: %H:%i') as creation_time, comment.content, comment.id
            FROM comment 
            INNER JOIN user ON comment.user_id=user.id 
            WHERE user.username=:username
            ORDER BY comment.creation_time DESC;";

        global $pdo;
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":username", $selected_user, PDO::PARAM_STR);
            if ($stmt->execute()) {
                if ($stmt->rowCount() > 1) {
                    echo "<tr class='data-table-header'>";
                    echo "<td class='data-table-cell-first'>";
                    echo "Date";
                    echo "</td>";
                    echo "<td class='data-table-cell'>";
                    echo "Comment";
                    echo "</td>";
                    echo "<td class='data-table-cell'>";
                    echo "Upvotes";
                    echo "</td>";
                    echo "<td class='data-table-cell'>";
                    echo "Downvotes";
                    echo "</td>";

                    echo "</tr>";
                    while ($row = $stmt->fetch()) {
                        $comment = $row["content"];
                        $date = $row["creation_time"];
                        $upvotes = get_vote_count($row["id"], true);
                        $downvotes = get_vote_count($row["id"], false);
                        echo "<tr class='data-table-row'>";
                        echo "<td class='data-table-cell-first'>";
                        echo $date;
                        echo "</td>";
                        echo "<td class='data-table-cell'>";
                        echo $comment;
                        echo "</td>";
                        echo "<td class='data-table-cell'>";
                        echo $upvotes;
                        echo "</td>";
                        echo "<td class='data-table-cell'>";
                        echo $downvotes;
                        echo "</td>";
                        echo "</tr>";
                    }
                }
            }

            unset($stmt);
        }
        echo "</table>";
    } else {
        echo "<h2>" . $selected_user . " does not exist</h2>";
    }
    ?>
</div>
</body>
</html>
