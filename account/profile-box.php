<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/account/login-functions.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/logging.php";

function echo_nav_bar()
{
    session_go();
    if (!is_logged_in()) {
        ip_login();
    }

    $post_text = "user";
    $user_type = $_SESSION["user_type"];
    if($user_type === "1"){
        $post_text = "auto-generated user";
    }
    else if($user_type ==="2"){
        $post_text = "admin";
    }
    else if($user_type ==="3"){
        $post_text = "personal";
    }
//    for some fucking reason i can't access home.png in icon folder when i move it there
    echo "<a class='link' href='/index.php'><img width=30 src='/images/home.svg' alt='Home button'></a>";
    echo "&nbsp;&nbsp;|&nbsp;&nbsp;";
//    echo "<br>";
    echo "<a class='link' href='/user/" . $_SESSION["username"] . "'>" . $_SESSION["username"] . "</a> - <span> " . $post_text . "</span>";
}

?>
<!--<link rel="stylesheet" href="/css/stylesheet.css">-->

<div class="profile">
    <?php echo_nav_bar()?>
</div>

<!--help i do not how to remove the scrollbar without javascript-->
<div class="profile-hidden-dynamic-magic-number-ffs">
    <?php echo_nav_bar(); ?>
</div>
