<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/error-box.php";

$selected_user = "Anonymous";
if (isset($_GET["user"])) {
    $selected_user = $_GET["user"];
}

$user_exists = does_user_exist($selected_user);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Users</title>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link rel="stylesheet" href="/css/table-style.css">
</head>
<body class="center-horizontal-parent">
<div class="center-child">

    <?php
    if ($user_exists) {
        echo "<h2>Log of " . $selected_user . "</h2>";
        echo "<table>";

        $sql = "SELECT visited_relative_url,DATE_FORMAT(log.creation_time, '%e/%m/%y: %H:%i') AS creation_time FROM log 
            WHERE user_id=(SELECT user_id FROM user WHERE username=:username) ORDER BY creation_time DESC;";

        global $pdo;
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":username", $selected_user, PDO::PARAM_STR);
            if ($stmt->execute()) {
                if ($stmt->rowCount() > 1) {
                    echo "<tr class='data-table-header'>";
                    echo "<td class='data-table-cell-first'>";
                    echo "Date";
                    echo "</td>";
                    echo "<td class='data-table-cell'>";
                    echo "Visited relative URL";
                    echo "</td>";
                    echo "</tr>";
                    while ($row = $stmt->fetch()) {
                        $rel_url = $row["visited_relative_url"];
                        $date = $row["creation_time"];
                        echo "<tr class='data-table-row'>";
                        echo "<td class='data-table-cell-first'>";
                        echo $date;
                        echo "</td>";
                        echo "<td class='data-table-cell'>";
                        echo $rel_url;
                        echo "</td>";
                        echo "</tr>";
                    }
                }
            }

            unset($stmt);
        }
        echo "</table>";
    } else {
        echo "<h2>" . $selected_user . " does not exist</h2>";
    }
    ?>
</div>
</body>
</html>
