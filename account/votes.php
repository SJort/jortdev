<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/error-box.php";

$selected_user = "Anonymous";
if (isset($_GET["user"])) {
    $selected_user = $_GET["user"];
}

$user_exists = does_user_exist($selected_user);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Votes of <?php echo $selected_user?></title>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link rel="stylesheet" href="/css/table-style.css">
</head>
<body class="center-horizontal-parent">
<div class="center-child">
    <?php
    if ($user_exists) {
        echo "<h2>Votes of " . $selected_user . "</h2>";
        echo "<table>";

        $sql = "SELECT DATE_FORMAT(comment_link.creation_time, '%e/%m/%y: %H:%i') as creation_time, user.username, comment.content, comment_link.link_type 
            FROM comment_link 
            INNER JOIN comment ON comment_link.comment_id=comment.id 
            INNER JOIN user ON comment_link.user_id=user.id 
            WHERE user.username=:username
            ORDER BY comment_link.creation_time DESC;";

        global $pdo;
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":username", $selected_user, PDO::PARAM_STR);
            if ($stmt->execute()) {
                if ($stmt->rowCount() > 1) {
                    echo "<tr class='data-table-header'>";
                    echo "<td class='data-table-cell-first'>";
                    echo "Date";
                    echo "</td>";
                    echo "<td class='data-table-cell'>";
                    echo "Comment";
                    echo "</td>";
                    echo "<td class='data-table-cell'>";
                    echo "User";
                    echo "</td>";
                    echo "<td class='data-table-cell'>";
                    echo "Action";
                    echo "</td>";
                    echo "</tr>";
                    while ($row = $stmt->fetch()) {
                        $date = $row["creation_time"];
                        $content = $row["content"];
                        $user = "<a class=link href=/user/" . $row["username"] . ">" . $row["username"] . "</a>";
                        $action = $row["link_type"] === "1" ? "Upvoted" : "Downvoted";
//                        only links should be green
//                        $action = "<span ". ($row["link_type"] === "1" ? "class=text-ok>Upvoted" : ">Downvoted") . "</span>";
                        echo "<tr class='data-table-row'>";
                        echo "<td class='data-table-cell-first'>";
                        echo $date;
                        echo "</td>";
                        echo "<td class='data-table-cell'>";
                        echo $content;
                        echo "</td>";
                        echo "<td class='data-table-cell'>";
                        echo $user;
                        echo "</td>";
                        echo "<td class='data-table-cell'>";
                        echo $action;
                        echo "</td>";
                        echo "</tr>";
                    }
                }
            }

            unset($stmt);
        }
        echo "</table>";
    } else {
        echo "<h2>" . $selected_user . " does not exist</h2>";
    }
    ?>
</div>
</body>
</html>
