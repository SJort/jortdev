<!--this could be loaded on each page, where an error could be shown-->
<?php
$error = "";
function getError()
{
    global $error;
    return $error;
}

function setError($errorToSet)
{
    global $error;
    $error = $errorToSet;
}

?>

<style>
    .error {
        bottom: 0;
        position: fixed;
        color: red;
        margin: auto;
    }
</style>

<div class="error">
    <?php
    if ($error != "") {
        echo "Error: ", $error;
    }
    ?>
</div>
