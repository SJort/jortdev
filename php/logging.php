<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/php/config.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/account/login-functions.php";

log_current_page();

function goto_previous_page()
{
    if (!is_logged_in()) {
        return false;
    }

    $sql = "SELECT * FROM log WHERE visited_relative_url != :rurl and user_id=:uid ORDER BY creation_time DESC LIMIT 1;";

    global $pdo;
    if ($stmt = $pdo->prepare($sql)) {

        $relative_url = "$_SERVER[REQUEST_URI]"; //not sure about the quotes
        $user_id = $_SESSION["user_id"];

        $stmt->bindParam(":uid", $user_id, PDO::PARAM_STR);
        $stmt->bindParam(":rurl", $relative_url, PDO::PARAM_STR);

        if ($stmt->execute()) {
            if ($stmt->rowCount() >= 1) {
                if ($row = $stmt->fetch()) {
                    $previous_url = $row["visited_relative_url"];
                    header("Location: " . $previous_url);
                    unset($stmt);
                    return true;
                }
            }
        }
    }
    unset($stmt);
    return false;
}

/**
 * Only call this function from within this file, otherwise you'll have to hardcode the column value on different places.
 * @param $user_id Int The user who the log belongs for.
 * @param $column String The column of the log table which needs to be written.
 * @param $value String The value to be written.
 * @return bool True if the log was written to the database.
 */
function log_current_page()
{
    if (!is_logged_in()) {
        return false;
    }
//    echo "log current page";

    $user_id = $_SESSION["user_id"];

    $sql = "INSERT INTO log (user_id, visited_full_url, visited_relative_url, ip_address) VALUES (:user_id, :furl, :rurl, :ip);";


    global $pdo;
    if ($stmt = $pdo->prepare($sql)) {

        $relative_url = "$_SERVER[REQUEST_URI]";
        $full_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $relative_url;
        $ip_address = $_SERVER['REMOTE_ADDR'];

        $stmt->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $stmt->bindParam(":rurl", $relative_url, PDO::PARAM_STR);
        $stmt->bindParam(":furl", $full_url, PDO::PARAM_STR);
        $stmt->bindParam(":ip", $ip_address, PDO::PARAM_STR);

        if ($stmt->execute()) {
            unset($stmt);
            return true;
        }
    }
    unset($stmt);
    return false;

}

