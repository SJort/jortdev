document.addEventListener('DOMContentLoaded', function () {
    onLoad();
}, false);

function onLoad() {
    let imageObjects = convertImagesJsonToObjectArray(json);
    loadGallery(imageObjects, folder);
}

function convertImagesJsonToObjectArray(response) {
    let result = [];
    let folder = Object.keys(response)[0];
    let images = response[folder];
    images.forEach(function (imageObject) {
        let fileName = imageObject["filename"];
        let imagePath = "/minecraft/" + folder + "/" + fileName;
        let width = imageObject["width"];
        let height = imageObject["height"];
        let item = {
            src: imagePath,
            w: width,
            h: height,
            pid: fileName,
        };
        result.push(item);
    });
    return result;
}


function loadGallery(items, folder) {
    let pswpElement = document.querySelectorAll('.pswp')[0];

    let cookieName = folder + "-index";
    let leftAt = Cookies.get(cookieName);
    if (!leftAt) {
        leftAt = 0;
    } else {
        leftAt = parseInt(leftAt);
    }

    let options = {
        index: leftAt, //start where left off
        pinchToClose: false,
        closeOnScroll: false,
        // closeOnVerticalDrag: false,
        // escKey: false,
        history: false,
        galleryPIDs: true,
        galleryUID: folder,
        shareButtons: [
            {id: 'telegram', label: 'Telegram', url: 'https://telegram.me/share/?text={{text}}&url={{url}}'},
            {id: 'download', label: 'Download image', url: '{{raw_image_url}}', download: true},
        ],
        preload: [2, 5],
    };

    let pswp = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    pswp.listen('close', function () {
        console.log("Closing folder...");
        window.close();
    });
    pswp.listen('beforeChange', function () {
        Cookies.set(cookieName, pswp.getCurrentIndex());
    });
    pswp.init();
}
