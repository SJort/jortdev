<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Jort</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

</head>
<body>
<div class="center-parent">
    <div class="center-child">
        <h1>Minecraft</h1>
        <ul>
            <li>
                <a class="link" href="/minecraft/images/spawn.png">Spawn</a>
                <br>
                XZ: -72, 172
            </li>
            <li>
                <a class="link" href="/minecraft/images/nether-spawn.png">Nether spawn</a>
                <br>
                XYZ: -9, 51, 20
            </li>
        </ul>
        <p>Join us at <a class="link" href="https://jort.dev">jort.dev</a>.</p>
        <a class="link-button" href="/minecraft/gallery.php" target="_blank">Gallery</a>
        <a class="link-button link-button-inverse" href="mailto:jortdeveloper@gmail.com">Contact Me</a>
    </div>
</div>

</body>
</html>
