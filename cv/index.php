<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/logging.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Curriculum Vitae</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

</head>
<body>
<div class="center-horizontal-parent">
    <div class="center-child">
        <h1>Curriculum Vitae</h1>
        I am comfortable with the following development tools.
        <ul>
            <li>
                <b>Java</b><br>
                The first programming language I have mastered.<br>
                I learned a lot from programming bots for a videogame:
                <ul>
                    <li>Writing dynamic GUI's</li>
                    <li>Using Graphics to paint a 2D field</li>
                    <li>How beautiful Interfaces are</li>
                    <li>Working with API's</li>
                    <li>The importance of comments</li>
                    <li>Overall programming skills which can be applied in most languages</li>
                </ul>
                The code of one of the bots can be found <a class="link" href="https://gitlab.com/SJort/projectwilderness">here</a>.
<!--                add barstbank-->

            </li>

            <br>

            <li>
                <b>JavaScript</b><br>
                The second language I have worked a lot with.<br>
                An example of the front end of a presence system I've coded can be found <a class="link" href="https://youtu.be/M1OLieCI5mY">here</a>.
            </li>

            <br>

            <li>
                <b>Python</b><br>
                A language I've used for machine learning with Tensorflow and side projects.
                <ul>
                    <li>AI controller for a self driving car in TORCS: <a class="link" href="https://gitlab.com/SJort/machine-learning">Link</a></li>
                </ul>
                <ul>
                    <li>Application to sort photos with shortcuts: <a class="link" href="https://gitlab.com/SJort/photosorter">Link</a></li>
                </ul>
                <ul>
                    <li>Screen overlay to control the mouse with the keyboard: <a class="link" href="https://gitlab.com/SJort/vimouse">Link</a></li>
                </ul>
            </li>

            <br>

            <li>
                <b>LAMP stack</b><br>
                The Linux Apache MySQL PHP stack used to make this site from scratch.<br>
                The development process can be found <a class="link" href="https://gitlab.com/SJort/jortdev">here</a>.<br>
                The following applies for this site.
                <ul>
                    <li>Runs at a home server with Arch Linux</li>
                    <li>Login/registration</li>
                    <li>Automatic login</li>
                    <li>Logging</li>
                    <li>Server folder parsing</li>
                    <li>Dynamic content pages</li>
                    <li>Clear URL's</li>
                    <li>Mobile friendly</li>
                    <li>Lightweight, no frameworks (except photoviewer)</li>
                </ul>
            </li>

        </ul>
    </div>
</div>

</body>
</html>
