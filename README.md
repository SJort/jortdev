# About
Code for my website: [jort.dev](https://jort.dev).  
Design recreated from [jschr.io](https://jschr.io/).  

# Setup guide for Arch Linux
A fresh Arch Linux installation is assumed in this guide.  

### Install required packages
```shell
sudo pacman -S apache php php-apache php-cgi
```
The php-cgi package is needed for Intellij.  

### Start apache
```shell
sudo systemctl enable --now httpd
```

### Clone git repository
```shell
mkdir /home/jort/this
cd /home/jort/this
git clone git@gitlab.com:SJort/jortdev.git
```

### Setup access rights for cloned directory
Check which group Apache runs as:
```shell
ps aux | less
```
Search for 'httpd'. The group is the first column, http in my case.
Change the group of the home and cloned directory to http:
```shell
sudo chgrp http /home/jort
sudo chgrp http /home/jort/this/jortdev
```
Change the home folder permission from the default 700 to 710:
```shell
chmod g+x /home/jort
```
You can double check those permissions with:
```shell
stat -c %a /home/jort
```

### Edit Apache configuration
Make a backup:
```shell
sudo cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf-backup
```
```shell
sudo vim /etc/httpd/conf/httpd.conf
```

#### Enable redirections
Uncomment:  
```apache
#LoadModule rewrite_module modules/mod_rewrite.so  
```

#### Enable PHP
Comment:  
```apache
LoadModule mpm_event_module modules/mod_mpm_event.so 
```
Uncomment:  
```apache
#LoadModule mpm_prefork_module modules/mod_mpm_prefork.so  
```
Add to the end of the LoadModule list:  
```apache
LoadModule php7_module modules/libphp7.so
AddHandler php7-script .php
```
Add to the end of the Include list:
```apache
Include conf/extra/php7_module.conf
```
Edit PHP configuration file:
```shell
sudo vim /etc/php/php.ini
```
Set to 'On':
```ini
display_errors = Off
```

#### Change server root to cloned directory
```apache
DocumentRoot "/home/jort/this/jortdev"
<Directory "/home/jort/this/jortdev">
```

#### Setup redirections
Setup the redirections within:
```apache
<Directory "/home/jort/this/jortdev">
...
</Directory>
```
These can be found in this repository in the config folder.

#### Apply changes
Apply changes to httpd.conf with:
```shell
sudo systemctl restart httpd
```

### Setup MariaDB
#### Install, setup and start MariaDB
```shell
sudo pacman -S mariadb
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl enable --now mariadb
```

#### Login:
```shell
sudo mysql -u root
```

#### Change the root password to root:
```sql
use mysql;
FLUSH PRIVILEGES;
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('root');
exit
```
Now you can login in SQL with:
```shell
sudo mysql -u root -proot
```
Paste the database creation queries there, which you can find in this repository in the db folder.

#### Setup PHP to work with MariaDB
Edit PHP configuration file:
```shell
sudo vim /etc/php/php.ini
```
Uncomment:
```ini
;extension=pdo_mysql
;extension=mysqli
```
Restart Apache:
```shell
sudo systemctl restart httpd
```

### Setup HTTPS
SSL certificate from [letsencrypt.org](https://letsencrypt.org).  
Certificate installed with [certbot.eff.org](https://certbot.eff.org/).  

#### Install Certbot
```shell
sudo pacman -S certbot certbot-apache
```
#### Edit Apache configuration
```shell
sudo vim /etc/httpd/conf/httpd.conf
```
Uncomment:  
```apache
#Include conf/extra/httpd-vhosts.conf 
#LoadModule rewrite_module modules/mod_rewrite.so  
#LoadModule ssl_module modules/mod_ssl.so
```
#### Edit Apache virtual host configuration
```shell
sudo vim /etc/httpd/conf/extra/httpd-vhosts.conf
```
Add virtual host:
```apache
<VirtualHost *:80>
    ServerName jort.dev
    RewriteEngine on
    RewriteCond %{SERVER_NAME} =jort.dev
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
```
#### Install certificate
Port 433 needs to be open for HTTPS.  
Run Certbot:
```shell
sudo certbot --apache
```
Setup automatic certificate renewal:
```shell
echo "0 0,12 * * * root python -c 'import random; import time; time.sleep(random.random() * 3600)' && certbot renew" | sudo tee -a /etc/crontab > /dev/null
```
Restart Apache:
```shell
sudo systemctl restart httpd
```

### Securing server
The server is constantly under attack, see:
```shell
journalctl -u sshd | grep Failed
```
To automatically ban IP addresses with suspicious behaviour:
```shell
sudo pacman -S fail2ban
sudo mkdir /etc/fail2ban
sudo vim /etc/fail2ban/jail.local
```
Enter the following configuration in the jail.local file:
```conf
[DEFAULT]
bantime = 1d

[sshd]
enabled = true
```
Start the banning service:
```shell
sudo systemctl enable --now fail2ban
```

The better solution against attacks is to only allow public and private key connections instead of passwords.

# Troubleshooting
## Can SSH in server, but server itself cannot access internet
### Symptoms
* Can SSH in server locally and remotely
* dhcpcd is running (systemctl list-unit-files)
* Can't ping google.com or install packages etc
* Can ping 8.8.8.8
* /etc/resolv.conf is empty or only contains comments
* Hostnames are correctly configures in /etc/hosts and /etc/hostname (they are the same)

### Solution
Because 8.8.8.8 is pingable, internet drivers are working.
When pinging google.com etc, a DNS server is used to determine the IP behind google.com.
Google.com cannot be accessed, so there must be something wrong with the DNS servers.  

openresolv automatically copies an IP from /etc/dhcpcd.conf to /etc/resolv.conf.
/etc/resolv.conf was empty, so this did not happen.
For me, openresolv was somehow uninstalled, probably during a system update.
To fix: manually populate openresolv.
In /etc/dhcpcd.conf, the static domain_name_server=192.168.1.1
So in /etc/openresolv. I added: nameserver 192.168.1.1
This gets reset when dhcpcd is restarted, so I installed the openresolv package, which populates the file automatically.

