<?php
function echo_line(){
    $result = "";
    for($i = 0; $i < 40; $i++){
        $result .= "LongLine" . $i;
    }
    echo $result;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Jort - test page 1</title>
    <style>
        .center-parent {
            display: grid;
            place-items: center;
            height: 100vh;
            padding: 0px 10px 0px 10px; /*prevent text to touch border on smaller screens*/
        }

        /*same but for horizontal center only*/
        .center-horizontal-parent {
            display: grid;
            place-items: start center;
        }
        /*PROFILE BOX/////////////////////////////////////////////////////////////////////////////////////////////////////*/
        .profile {
            top: 0;
            right: 0;
            left: 0;
            position: fixed;
            text-align: right;
            background-color: lightblue;
        }

        *{
            font-size: 16px;
            word-break: break-word;
        }
    </style>
</head>

<body>

<div class="profile">
    User Jort is logged in as administrator
</div>

<div class="center-horizontal-parent">

    <?php
    for($i = 0; $i < 4; $i++){
        echo "Content " . $i . "<br>";
    }
    ?>
    The above starts with 0, but it is overlapped <br>
    <br>
    Content: centered horizontally and vertically in the remaining space of navigation,
    only introduce scrollbar when not fitting<br>
    <table>
        <tr>
            <td>
                Comment1
            </td>
        </tr>
        <tr>
            <td>
                Comment2
                <table>
                    <tr>
                        <td>
                            Reply to 2
                            <?php echo_line() ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
</div>


</body>
</html>

