<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jort - NSA</title>
    <script src="/js/base-devel.js"></script>
</head>
<body>
<div id="j-output">

</div>
<a href="https://ip-api.com/docs/api:json#test">IP API</a>

<script>
    let ip = "<?php echo $_SERVER['REMOTE_ADDR'] ?>";
    let height = window.screen.availHeight;
    let width = window.screen.availWidth;
    let agent = navigator.userAgent;
    let long = "Nothing";
    let lat = "Nothing";

    function showPosition(position) {
        long = position.coords.longitude;
        lat = position.coords.latitude;
    }
    navigator.geolocation.getCurrentPosition(showPosition);



    let output = document.getElementById("j-output");
    output.innerHTML += "<br>IP: " + ip;
    output.innerHTML += "<br>Height: " + height;
    output.innerHTML += "<br>Width: " + width;
    output.innerHTML += "<br>Agent: " + agent;

    output.innerHTML += "<br>Longitude: " + long;
    output.innerHTML += "<br>Latitude: " + lat;


    let query = "http://ip-api.com/json/" + ip + "?fields=66846719";
    log("Query: " + query);

    let request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let json = JSON.stringify(JSON.parse(this.responseText), null, 2);

            output.innerHTML += "<br>JSON: " + json;

        }
    };
    request.open("GET", query, true);
    request.send();

</script>
</body>
</html>


