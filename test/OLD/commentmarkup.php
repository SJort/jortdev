<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Comments</title>
    <script>
        function toggle(button) {
            let hh = document.getElementById("form" + button.id);
            if (hh.style.display === "none") {
                hh.style.display = "block";
                button.innerHTML = "Close"
            } else {
                hh.style.display = "none";
                button.innerHTML = "Reply"
            }
        }
    </script>
    <style>
        table {
            border: solid;
            margin: auto;
        }
    </style>

</head>
<body>


<table>
    <tr>
        <td>
            Username1
        </td>
        <td>
            <button>Reply</button>
        </td>
    </tr>
    <tr>
        <td>
            Comment1
        </td>
    </tr>
    <tr>
        <td>
            Reply1 HTML
        </td>
    </tr>
    <tr>
        <td>
            <hr>
        </td>
    </tr>
    <tr>
        <td>
            Username2
        </td>
        <td>
            <button>Reply</button>
        </td>
    </tr>
    <tr>
        <td>
            Comment2
        </td>
    </tr>
    <tr>
        <td>
            Reply2 HTML
        </td>
    </tr>
</table>

</body>

