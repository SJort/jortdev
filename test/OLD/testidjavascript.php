<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>The Testing Environment</title>
    <script>
        function toggle(button){
            let hh = document.getElementById("form" + button.id);
            if(hh.style.display === "none"){
                hh.style.display = "block";
            }
            else{
                hh.style.display = "none";
            }
        }

    </script>
    <style>

    </style>
</head>
<body>


<div id="form2" style="display:block">
    HTML to leave comment2
</div>

<button id="2" onclick="toggle(this)">
    Comment2
</button>

<div id="form3" style="display:block">
    HTML to leave comment3
</div>

<button id="3" onclick="toggle(this)">
    Comment3
</button>


</body>
</html>
