<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dsn = 'mysql:dbname=kevinbergenhen_website;host=localhost;port=3306;charset=utf8';

//SWITCH THESE FOR TESTING LOCAL
//$connection = new \PDO($dsn, "kevinbergenhen", "xJjKQm4fr9j1");
$connection = new \PDO($dsn, "root", "root");

// throw exceptions wanneer je een SQL error krijgt
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);


$response = new \stdClass();
$response->{"response"} = "Error: invalid request.";

//http://localhost/test/testsessionserver?request=setPassword&password=hoii
if ($_GET["request"] == "setPassword" && ISSET($_GET["password"])) {
    $password = $_GET["password"];

    $query = "select * from users where password='" . $password . "';";

    $response->{"query"} = $query;

    $statement = $connection->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    if ($result == []){
        $response->{"response"} = "Access denied.";
        session_unset();
    }
    else {
        $response->{"response"} = "Access granted.";
        $_SESSION["password"] = $password;
    }
}

$json = json_encode($response);
echo $json;
