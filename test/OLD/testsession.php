<?php
session_start();
if(ISSET($_SESSION["password"])) {
    echo "Acces granted.";
}
else{
    echo "Access denied";
}
?>

<html>
 <head>
  <title>PHP Test</title>
 </head>
 <body>
 <?php echo '<p>Hello World</p>'; ?>

 <script>
     function handlePasswordInput(){
         let password = prompt("Voer wachtwoord in");
         if(password !== null){
            //http://localhost/test/testsessionserver?request=setPassword&password=hoii
             let request = "testsessionserver?request=setPassword&password=" + password;
             console.log("Request: " + request);

             fetch(request)
                 .then((response) => {
                     // return response.json(); //must be separate promise because .json is also a promise..?
                     return response.json();
                 })
                 .then((response) => {
                     console.log("Request result: " + JSON.stringify(response));
                     if(response["response"].includes("granted")){
                         alert("Access granted.");
                     }
                     else {
                         alert("Access denied.");
                     }
                 })
                 .catch(err => {
                     console.log("Error with request:");
                     console.log(err);
                 });
         }
     }
 </script>
 <button onclick="handlePasswordInput()">Authenticate</button>
 </body>
</html>
