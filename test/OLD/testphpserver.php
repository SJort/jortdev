<?php
if ($_GET["request"] == "test") {
    echo "Requests working!";
}

if ($_GET["request"] == "getFolders") {
    $folders_path = "../images";
    $folders = array_diff(scandir($folders_path), array('.', '..'));

    $json_main = new \stdClass();
    $json_main->{"folders"} = array();

    foreach ($folders as $folder) {
        $image_folder_path = $folders_path . "/" . $folder;
        if (!is_dir($image_folder_path)) {
            continue;
        }
        array_push($json_main->{"folders"}, $folder);
    }
    $json = json_encode($json_main);
    echo($json);
}

if ($_GET["request"] == "getAll") {
    $folders_path = "../images";
    $folders = array_diff(scandir($folders_path), array('.', '..'));

    $json_main = new \stdClass();
    foreach ($folders as $folder) {
        $image_folder_path = $folders_path . "/" . $folder;
        if (!is_dir($image_folder_path)) {
            continue;
        }

        $key = "images/" . $folder . "/";
        $json_main->{$key} = array();

        $image_folder = array_diff(scandir($image_folder_path), array('.', '..'));
        foreach ($image_folder as $image_filename) {
            array_push($json_main->{$key}, $image_filename);
        }
    }
    $json = json_encode($json_main);
    echo($json);
}

