<?php
//$dsn = 'mysql:dbname=prescant;host=localhost;port=3306;charset=utf8';
$dsn = 'mysql:dbname=kevinbergenhen_website;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions wanneer je een SQL error krijgt
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

if($_GET["request"] == "test") {
    $statement = $connection->prepare("select * from events");
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($result);
    echo $json;
}
elseif($_GET["request"] == "getEvents") {

    echo "hoi";
}
else{
    echo "No request set.";
}
