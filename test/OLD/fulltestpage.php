<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>The Testing Environment</title>
    <script>

    </script>
    <style>
        nav { /*apply to the header within the body*/
            flex: 0 1 auto; /*grow: 0, shrink: 1, basis: auto*/
            background-color: lightblue;
            text-align: right;
        }

        body {
            display: flex;
            flex-flow: column;
            height: 100vh;
            background-color: lightcyan;
            margin: 0;
        }

        .content { /*apply to the box containing everything but the header*/
            flex: 1 1 auto;
            align-content: center;
        }

    </style>

</head>

<nav>
    NAVIGATION<br>
    I want this in the top right (already fully correct in this example)<br>
    The navigation bar must be fixed/sticky, so always visible when scrolling.<br>
</nav>
<body>
    <div class="content">
        CONTENT<br>
        I want this centered vertically and horizontally.<br>
        The whole page may not contain a scrollbar, unless the content does not fit anymore.<br>
        The content may not overlap with the navigation bar.
    </div>
</body>
</html>
