document.addEventListener('DOMContentLoaded', function () {
    onLoad();
}, false);

function log(message) {
    console.log(message);
}

function parse() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            console.log(this.responseText);
            let json = JSON.parse(this.responseText);
            let container = document.getElementById("content");
            let result = "";
        }
    };
    request.open("GET", "testphpserver.php?request=getAll", true);
    request.send();
}

function loadImages(json) {
    log("Loading images...");
    let images = document.getElementById("images");
    Object.keys(json).forEach(function(folderKey){
        let folder = json[folderKey];
        Object.keys(folder).forEach(function(imageKey){
            let path = folderKey + folder[imageKey];
            path = "../" + path;
            let image = document.createElement("img");
            image.setAttribute("src", path);
            image.setAttribute("height", "200");
            images.appendChild(image);
        });
    });
}

function handleResponse(json) {
    log("Response: " + json + " (" + typeof(json) + ")");
    Object.keys(json).forEach(function(folderKey){
        log(folderKey);
        let folder = json[folderKey];
        Object.keys(folder).forEach(function(imageKey){
            log("--" + imageKey + " (" + folder[imageKey] + ")");
        });
    });
}

function onLoad() {
    log("onLoad()");
    fetch("testphpserver.php?request=getAll")
        .then((response) => {
            return response.json(); //must be separate promise because .json is also a promise..?
        })
        .then((response) => {
            handleResponse(response);
            loadImages(response);
        });
}

