<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Centering div test</title>
    <style>
        html, body{
            margin: 0;
        }
        .navbar {
            top: 0;
            left: 0;
            right: 0;
            position: fixed;
            text-align: right;
            background-color: lightcyan;
        }

        .navbar-dynamic-magic-number {
            top: 0;
            left: 0;
            right: 0;
            text-align: right;
            background-color: lightcyan;
            visibility: hidden;
        }

        .center {
            display: grid;
            place-items: center;
            height: 100vh;
            padding: 0px 10px 0px 10px; /*prevent text to touch border on smaller screens*/
            background-color: lightpink;
        }

        .center-horizontal {
            display: grid;
            place-items: start center;
            height: 100vh;
            padding: 0px 10px 0px 10px; /*prevent text to touch border on smaller screens*/
            background-color: lightpink;
        }
    </style>

</head>
<div class="navbar">
    Navigation: fixed, top right, not overlapping with centered content
</div>

<div class="navbar-dynamic-magic-number">
    Navigation: fixed, top right, not overlapping with centered content
</div>
<body>
<div class="center-horizontal">
    <?php
        for($i = 0; $i < 40; $i++){
            echo "Content " . $i . "<br>";
        }
    ?>
    The above starts with 0, but it is overlapped <br>
    <br>
    Content: centered horizontally and vertically in the remaining space of navigation,
    only introduce scrollbar when not fitting<br>
</div>

</body>
</html>
