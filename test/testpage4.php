<!DOCTYPE HTML>
<html>
<head>
    <script>
        navigator.permissions.query({name:'accelerometer'}).then(function(result) {
            if (result.state === 'granted') {
                alert("Granted")
            } else if (result.state === 'prompt') {
                alert("Prompt")
            }
            // Don't do anything if the permission was denied.
        });
    </script>
</head>
<body>
<div id="chartContainer" style="height: 370px; width:100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>
