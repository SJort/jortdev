document.addEventListener('DOMContentLoaded', function () {
    onLoad();
}, false);


function updateCookie(folder, read) {
    htmlLog("Updating cookie: " + folder + " read: " + read);
    let selected = Cookies.get("read");
    if (!selected) {
        selected = "";
    }
    // log("Selected: " + selected);
    if (selected.includes(folder) === read) {
        // log("Folder already properly in cookie");
        return;
    }
    if (read) {
        selected += folder + ", ";
    } else {
        selected = selected.replace(folder + ", ", "");
    }
    // log("Selected after: " + selected);
    Cookies.set("read", selected, {expires: 365});
}

function handleFoldersResponse(json) {
    let folders = json["folders"];
    let table = document.getElementById("table");
    folders.forEach(function (folder) {
        htmlLog("Iterating: " + folder);
        //retrieve the just inserted row, variable is initialized in scope because checkbox listener needs a unique variable
        let currentRow = table.insertRow();

        //create empty cells in the just inserted row
        for (let column = 0; column < 3; column++) {
            currentRow.insertCell();
        }

        let link = document.createElement("a");
        let linkDestination = "/threads/" + folder;
        // linkDestination = "#";
        link.setAttribute("href", linkDestination);
        link.setAttribute("class", "link");
        link.innerHTML = folder;
        link.addEventListener("click", function () {
            updateCookie(folder, true);
        });
        currentRow.cells[0].appendChild(link);

        //presence checkbox
        //https://hunzaboy.github.io/CSS-Checkbox-Library/#
        let checkbox = document.createElement('input');
        checkbox.setAttribute("type", "checkbox");
        checkbox.setAttribute("id", folder);
        checkbox.setAttribute("value", "0");
        checkbox.setAttribute("checked", "");
        checkbox.setAttribute("name", "ckbx-square-1");

        let checkboxLabel = document.createElement("label");
        checkboxLabel.setAttribute("for", folder);

        let checkboxContainer = document.createElement("div");
        checkboxContainer.setAttribute("class", "ckbx-style-8 ckbx-medium");
        checkboxContainer.appendChild(checkbox);
        checkboxContainer.appendChild(checkboxLabel);


        //modified indicator, always there because otherwise when inserted on interaction, the table shifts
        let modifiedCell = currentRow.cells[2];
        modifiedCell.innerText = "modified";
        modifiedCell.setAttribute("style", "opacity: 0");

        // save initial value so we can determine if the value is altered
        if (Cookies.get("read") && Cookies.get("read").includes(folder)) {
            checkbox.setAttribute("originalValue", 1);
        } else {
            checkbox.setAttribute("originalValue", 0);
        }

        //set checkbox checked on basis of the saved initial value
        checkbox.checked = checkbox.getAttribute("originalValue") === "1";

        checkbox.addEventListener('change', function () {
            //handle the modified indicator:

            //if the original value matches the current value, hide the modified indicator
            if (checkbox.getAttribute("originalValue") === "1" && this.checked || checkbox.getAttribute("originalValue") === "0" && !this.checked) {
                //toggle visibility because its always there
                modifiedCell.setAttribute("style", "opacity: 0");
            }
            //else show the modified indicator
            else {
                modifiedCell.setAttribute("style", "opacity: 1;");
                //bootstrap theme color
                modifiedCell.className = "text-warning";
            }

            //update cookie with read threads
            updateCookie(folder, this.checked);
        });

        //add the created checkbox to the row
        currentRow.cells[1].appendChild(checkboxContainer);
    });
}

function showViewer(show) {
    let viewer = document.getElementById("viewer-root");
    if (show) {
        viewer.setAttribute("style", "");
    } else {
        viewer.setAttribute("style", "visibility: hidden; display:none;");
    }
    htmlLog("Showing viewer: " + show);
}

function loadFolders() {
    fetch("/photoviewer/server.php?request=getFolders")
        .then((response) => {
            return response.json(); //must be separate promise because .json is also a promise..?
        })
        .then((response) => {
            handleFoldersResponse(response);
        });
}

function onLoad() {
    htmlLog("onLoad()");
    htmlLog("Request: " + request);
    if (request === "") {
        htmlLog("Empty request, loading folders...");
        showViewer(false);
        loadFolders();
    } else {
        htmlLog("Loading images...");
        loadImages(request);
        loadFolders();
    }
}

function handleImagesResponse(response) {
    let result = [];
    let folder = Object.keys(response)[0];
    logVar(folder);
    let images = response[folder];
    images.forEach(function (imageObject) {
        let fileName = imageObject["filename"];
        let imagePath = "/threads/" + folder + "/" + fileName;
        let width = imageObject["width"];
        let height = imageObject["height"];
        let item = {
            src: imagePath,
            w: width,
            h: height,
            pid: fileName,

        };
        htmlLog("Pushing: " + JSON.stringify(item));
        result.push(item);
    });
    return result;
}

function loadImages(requestedFolder) {
    htmlLog("Loading images");
    Cookies.set("selected", requestedFolder);
    let selected = Cookies.get("selected");
    let imagesRequest = "/photoviewer/server.php?request=getImages&folder=" + selected;
    htmlLog("Request: " + imagesRequest);

    fetch(imagesRequest)
        .then((response) => {
            return response.json(); //must be separate promise because .json is also a promise..?
        })
        .then((response) => {
            return handleImagesResponse(response);
        })
        .then((response) => {
            loadGallery(response, request);
        })
        .catch(err => {
            htmlLog(err);
            htmlLog("Folder probably does not exist, redirecting...");
            sleep(1000);
            window.history.back();
            // window.location.replace("/threads");
        });
}

function loadGallery(items, folder) {
    htmlLog("Loading gallery with " + items);
    let pswpElement = document.querySelectorAll('.pswp')[0];

    let cookieName = folder + "-index";
    let leftAt = Cookies.get(cookieName);
    if (!leftAt) {
        leftAt = 0;
    } else {
        leftAt = parseInt(leftAt);
    }


    let options = {
        index: leftAt, //start where left off
        pinchToClose: false,
        closeOnScroll: false,
        // closeOnVerticalDrag: false,
        // escKey: false,
        history: false,
        galleryPIDs: true,
        galleryUID: folder,
        shareButtons: [
            {id: 'telegram', label: 'Telegram', url: 'https://telegram.me/share/?text={{text}}&url={{url}}'},
            {id: 'download', label: 'Download image', url: '{{raw_image_url}}', download: true},
        ],
        preload: [2, 5],
    };

    let pswp = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    pswp.listen('close', function () {
        htmlLog("Close detected, redirecting...");
        // window.location.replace("/threads")
        // window.history.pushState('Threads', 'Threads', '/threads');
        window.history.back();
    });
    pswp.listen('beforeChange', function () {
        Cookies.set(cookieName, pswp.getCurrentIndex());
    });
    pswp.init();
}
