<?php
$folders_path = "../images"; //does not work with absolute path

//&request=getImages&folder=folder1
if ($_GET["request"] == "getImages" && ISSET($_GET["folder"])) {
    $folders = array_diff(scandir($folders_path), array('.', '..'));
    $selected_folder = $_GET["folder"];

    $json_main = new \stdClass();
    foreach ($folders as $folder) {
        $image_folder_path = $folders_path . "/" . $folder;
        if (!is_dir($image_folder_path)) {
            continue;
        }
        if($folder != $selected_folder){
            continue;
        }

        $json_main->{$folder} = array();

        $image_folder = array_diff(scandir($image_folder_path), array('.', '..'));
        foreach ($image_folder as $image_filename) {
            $image_data = new \stdClass();
            $image_data->{"filename"} = $image_filename;
            if(!preg_match("/\.(?:jpg|png|gif)$/", $image_filename)){
                continue;
            }
            list($width, $height) = getimagesize($image_folder_path . "/" . $image_filename);
            $image_data->{"width"} = $width;
            $image_data->{"height"} = $height;
            array_push($json_main->{$folder}, $image_data);
        }
    }
    $json = json_encode($json_main);
    echo($json);
}

if ($_GET["request"] == "getFolders") {
    $folders = array_diff(scandir($folders_path), array('.', '..'));

    $json_main = new \stdClass();
    $json_main->{"folders"} = array();

    foreach ($folders as $folder) {
        $image_folder_path = $folders_path . "/" . $folder;
        if (!is_dir($image_folder_path)) {
            continue;
        }
        array_push($json_main->{"folders"}, $folder);
    }
    $json = json_encode($json_main);
    echo($json);
}
