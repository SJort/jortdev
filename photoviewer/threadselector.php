<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Select thread</title>

    <!--    import request from php to javascript-->
    <script>
        let request = "<?php echo $_GET['folder']?>";
    </script>

    <!--    photoswipe library: https://photoswipe.com -->
    <link rel="stylesheet" href="/libraries/gallery/photoswipe.css">
    <link rel="stylesheet" href="/libraries/gallery/default-skin/default-skin.css">
    <script src="/libraries/gallery/photoswipe.min.js"></script>
    <script src="/libraries/gallery/photoswipe-ui-default.min.js"></script>

    <script src="/js/base-devel.js"></script>
    <script src="/js/js.cookie.min.js"></script>
    <script src="/photoviewer/threadselector-main.js"></script>

    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/libraries/checkboxes.min.css">
</head>
<body>

<div class="center-horizontal-parent">
    <div class="center-child">
        <h1>Select thread</h1>
        <div id="threads"></div>
        <table id="table">
            <tr>
                <th>Thread</th>
                <th>Seen</th>
            </tr>
        </table>
    </div>
</div>

<!--photoswipe stuff with comments @ documentation-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true" id="viewer-root" >
    <div class="pswp__bg"></div>

    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>


</body>
</html>
