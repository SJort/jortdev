<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Jort</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

</head>
<body>
<div class="center-parent">
    <div class="center-child">
        <table>
            <tr><td><h1>Hi, I'm Jort.</h1></td></tr>
            <tr><td>A developer responsible for—<td></tr>
            <tr><td></td></tr>
        </table>

        <table>
            <?php
            function echo_list_item($url, $title, $description){
                echo "<tr>";
                echo "<td class='text-grey'>></td>";

                echo "<td>";
                echo "<a class=link href=". $url . ">" . $title . "</a>";
                echo "</td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td> </td>";
                echo "<td>";
                echo $description . "\n";
                echo "</td>";
                echo "</tr>";
//                add this spacing if using link-style-position: inside;
//                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n";
            }
            echo_list_item("/cv", "Curriculum Vitae", "The web version of my CV.");
            echo_list_item("/threads", "YLYL", "You Laugh You Lose images from 4chan.");
            echo_list_item("/comments", "Comments", "Leave a comment on this site, without registering.");
            echo_list_item("/sensor", "Sensors", "Insight in the values read from your device sensors.");
            echo_list_item("/account/register", "Register", "Create an account on this website.");
            echo_list_item("/user", "User list", "List of all the users on this site.");
            echo_list_item("/account/login", "Login", "Login with your account.");
            echo_list_item("/minecraft", "Minecraft", "My Minecraft server.");
            echo_list_item("/wop", "Work in progress", "Stuff I am working on right now.");
            ?>

        </table>
        <br>
        <p>I'm currently a student at <a class="link" href="https://www.hogeschoolrotterdam.nl/">HR</a>.</p>
        <a class="link-button" href="https://gitlab.com/SJort" target="_blank">My GitLab</a>
        <a class="link-button link-button-inverse" href="mailto:jortdeveloper@gmail.com">Contact Me</a>
    </div>
</div>

</body>
</html>
