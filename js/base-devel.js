//log in console and on website
function htmlLog(message) {
    let logBar = document.getElementById("logBar");

    if (logBar == null) {
        return;
    }
    logBar.innerText = message + "\n" + logBar.innerText;
}

//overwrite the console commands so log messages are intercepted and can be printed in html
function overwriteConsole() {
    (function () {
        let _log = console.log;
        let _error = console.error;
        let _warning = console.warning;

        console.error = function (errMessage) {
            htmlLog("Error: " + errMessage);// Send a mail with the error description
            _error.apply(console, arguments);
        };

        console.log = function (logMessage) {
            htmlLog("Log: " + logMessage);
            // Do something with the log message
            _log.apply(console, arguments);
        };

        console.warning = function (warnMessage) {
            htmlLog("Warning: " + warnMessage)
            // do something with the warn message
            _warning.apply(console, arguments);
        };

    })();
}

function log(message) {
    console.log(message);
}

function logVar(variable) {
    log("Variable: " + variable + " (" + typeof (variable) + ")")
}

function logObject(object) {
    log("Object: " + JSON.stringify(object));
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function goBackWithFallBack(fallbackUrl) {
    fallbackUrl = fallbackUrl || '/';
    let prevPage = window.location.href;
    window.history.go(-1);
    setTimeout(function () {
        if (window.location.href === prevPage) {
            console.log("Can't go back, using fallback...");
            window.location.replace(fallbackUrl);
        }
    }, 500);
}
