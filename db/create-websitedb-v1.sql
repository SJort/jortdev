-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema websitedb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema websitedb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `websitedb` ;
USE `websitedb` ;

-- -----------------------------------------------------
-- Table `websitedb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `websitedb`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mail` VARCHAR(254) NULL,
  `username` VARCHAR(16) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `type` TINYINT(0) NOT NULL DEFAULT 0,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `websitedb`.`comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `websitedb`.`comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `poster` VARCHAR(16) NOT NULL,
  `content` VARCHAR(255) NOT NULL,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_comment_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `websitedb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `websitedb`.`folder`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `websitedb`.`folder` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `websitedb`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `websitedb`.`image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(255) NOT NULL,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `folder_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_image_folder1`
    FOREIGN KEY (`folder_id`)
    REFERENCES `websitedb`.`folder` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `websitedb`.`favorite_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `websitedb`.`favorite_image` (
  `user_id` INT NOT NULL,
  `image_id` INT NOT NULL,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`),
  CONSTRAINT `fk_favorite_image_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `websitedb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_favorite_image_image1`
    FOREIGN KEY (`image_id`)
    REFERENCES `websitedb`.`image` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
