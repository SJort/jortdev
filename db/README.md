# Database
## Relations
* Image link and folder link indicate a relation between a folder/image and an user.
For example: the user has seen the image or has favorited the image.
Type in user indicates which type of user it is.
For example: an administrator or a regular user.

## Constants
### Table user
* type:  
    * 0 = Normal user
    * 1 = Auto created user (IP login)
    * 2 = Administrator
    * 3 = Personal
### Table comment_link
* link_type:
    * 1 = Upvote
    * 2 = Downvote


* In the links, for example the comment_link, the user_id and the comment_id form the primary key together, because you
should not be able to like a comment twice.

# Updating database
Use the default settings When forward engineering the model in MySQL Workbench.   
Remove all lines containing 'INDEX' in the result.
Because everything with 'INDEX' has to be removed, UNIQUE_INDEX stuff is not possible.
Paste it in the database command line:
```shell
sudo mysql -u root -proot
```
If it does not seem updated, drop the entire table:
```sql
DROP DATABASE websitedb;
```

# Adding values
Add user:
```sql
INSERT INTO user (mail, username, password, type)
VALUES ("jort@jort.dev", "Jort", "water", 2);
```
Add comment:
```sql
INSERT INTO comment (user_id, poster, content)
VALUES (2, "jortanon", "nice website");
```

# Workbench
Edit the default connection, set username and password to root.
Install gnome-keyring if org.freedesktop error.
