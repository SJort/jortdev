# Users
INSERT INTO user (username, password, type)
VALUES ("Lars1", "water", 1);

INSERT INTO user (username, password, type)
VALUES ("Peter2", "water", 1);

INSERT INTO user (username, password, type)
VALUES ("Jan3", "water", 1);

INSERT INTO user (username, password, type)
VALUES ("Klaas4", "water", 1);

INSERT INTO user (username, password, type)
VALUES ("Manfred5", "water", 1);

INSERT INTO user (username, password, type)
VALUES ("Jort", "water", 2);



# Comments
INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (1, "Parent comment", NULL);

INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (2, "Parent comment2", NULL);

INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (3, "Parent comment3", NULL);

INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (4, "Nice website", NULL);

INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (2, "Child comment", 2);

INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (4, "Child comment2", 2);

INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (5, "Child comment3", 2);

INSERT INTO comment (user_id, content, parent_comment_id)
VALUES (2, "ChildChild", 6);

# Votes
INSERT INTO comment_link (user_id, comment_id, link_type)
VALUES (2, 2, 1);

INSERT INTO comment_link (user_id, comment_id, link_type)
VALUES (3, 3, 2);



