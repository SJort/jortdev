
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema kevinbergenhen_website
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema kevinbergenhen_website
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `kevinbergenhen_website` ;
USE `kevinbergenhen_website` ;

-- -----------------------------------------------------
-- Table `kevinbergenhen_website`.`events`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kevinbergenhen_website`.`events` (
                                                                 `title` VARCHAR(45) NULL,
                                                                 `date` DATE NULL,
                                                                 `type` VARCHAR(45) NULL)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kevinbergenhen_website`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kevinbergenhen_website`.`users` (
                                                                `name` VARCHAR(128) NULL,
                                                                `password` VARCHAR(128) NULL)
    ENGINE = MEMORY;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO events (title, date, type) VALUES ("Comic Con", "2020-05-26", 1);
INSERT INTO events (title, date, type) VALUES ("Photoshoot met Kerel", "2020-05-27", 2);
INSERT INTO events (title, date, type) VALUES ("Vakantie", "2020-08-3", 2);

-- please dont hack
INSERT INTO users (name, password) VALUES ("Jort", "water");
INSERT INTO users (name, password) VALUES ("Kevin", "crater");






create database if not exists site;
use site;
CREATE TABLE if not exists users (
                       id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                       username VARCHAR(16) NOT NULL UNIQUE,
                       password VARCHAR(255) NOT NULL,
                       privilege TINYINT,
                       created_at DATETIME DEFAULT CURRENT_TIMESTAMP

);
# ALTER TABLE users ADD privilege TINYINT;

INSERT INTO users (username, password) VALUES ("Jort", "hoi");
INSERT INTO users (username, password) VALUES ("Tester", "test");
