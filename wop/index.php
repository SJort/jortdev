<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Work in Progress</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

</head>
<body>
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/logging.php";
?>

<div class="center-parent">
    <div class="center-child">
        <h1>Work in Progress</h1>
        <ul>
            <li>
                <a class="link" href="../account/log.php">Log</a>
            </li>
            <li>
                <a class="link" href="../test/testpage1.php">Test page 1</a>
            </li>
            <li>
                <a class="link" href="../test/testpage2.php">Test page 2</a>
            </li>
            <li>
                <a class="link" href="../test/testpage3.php">Test page 3</a>
            </li>

            <li>
                <a class="link" href="../test/testpage4.php">Test page 4</a>
            </li>
            <li>
                <a class="link" href="/test/OLD">Old tests</a>
            </li>
        </ul>
    </div>
</div>

</body>
</html>
