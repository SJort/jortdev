
function toggle(button) {
    let id = button.getAttribute("comment");

    //open the comment entry
    let replyDiv = document.getElementById("form-" + id);
    if (replyDiv.style.display === "none") {
        replyDiv.style.display = "block";
        button.innerHTML = "Close"
    } else {
        replyDiv.style.display = "none";
        button.innerHTML = "Reply"
    }

    //change the just clicked button
    if(button.src.includes("close")){
        button.src = "/images/reply.svg";
    }
    else{
        button.src = "/images/close.svg"

        //focus the opened text field
        let commentForm = document.getElementById("comment_field-" + id);
        commentForm.focus();
        // To update cursor position to recently added character in textBox
        commentForm.setSelectionRange(commentForm.innerHTML.length, commentForm.innerHTML.length);
    }
}

//Sends query to toggle vote on comment and visually adjusts the page the same way if it were to be reloaded
function vote(voteButton, upvoting) {
    let id = voteButton.getAttribute("comment");

    let voteType = upvoting ? "1" : "2";
    let query = "/comments/vote-server.php?request=toggle-vote&comment_id=" + id + "&vote_type=" + voteType;
    htmlLog("Voting query: " + query);

    let request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
        }
    };
    request.open("GET", query, true);
    request.send();

    //adjust button colors and counts
    //maybe put this in the response but may fuck up responsiveness
    let upvoteButton = document.getElementById("upvote-" + id);
    let downvoteButton = document.getElementById("downvote-" + id);

    let upvoteCounter = document.getElementById("upvotecount-" + id);
    let downvoteCounter = document.getElementById("downvotecount-" + id);

    let didUpvote = upvoteButton.src.includes("upvoted");
    let didDownvote = downvoteButton.src.includes("downvoted");

    //todo: adjust vote count
    if (upvoting) {
        if (didUpvote) { //untoggle if already voted
            upvoteButton.src = "/images/upvote.svg";
            upvoteCounter.innerHTML = parseInt(upvoteCounter.innerHTML) - 1;
        } else {
            upvoteButton.src = "/images/upvoted.svg";
            upvoteCounter.innerHTML = parseInt(upvoteCounter.innerHTML) + 1;
            if (didDownvote) {
                downvoteCounter.innerHTML = parseInt(downvoteCounter.innerHTML) - 1;
            }
        }
        downvoteButton.src = "/images/downvote.svg";

    } else { //downvoting
        if (didDownvote) { //untoggle if already voted
            downvoteButton.src = "/images/downvote.svg";
            downvoteCounter.innerHTML = parseInt(downvoteCounter.innerHTML) - 1;
        } else {
            downvoteButton.src = "/images/downvoted.svg";
            downvoteCounter.innerHTML = parseInt(downvoteCounter.innerHTML) + 1;
            if (didUpvote) {
                upvoteCounter.innerHTML = parseInt(upvoteCounter.innerHTML) - 1;
            }
        }
        upvoteButton.src = "/images/upvote.svg";

    }
}
