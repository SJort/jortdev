<?php
//dus comment #6 gets upvoted wanneer ik met user #6 vote op comment #1

require_once $_SERVER["DOCUMENT_ROOT"] . "/php/config.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/comments/vote-functions.php";

function echo_error($message)
{
    echo "Error in request: " . $message;
}

if ($_GET["request"] == "toggle-vote") {
    session_go();
    if (!is_logged_in()) {
        echo_error("Not logged in.");
        return;
    }

    if (!isset($_GET["vote_type"])) {
        echo_error("No vote type defined.");
        return;
    }

    if (!isset($_GET["comment_id"])) {
        echo_error("No comment id defined.");
        return;
    }

    $type = $_GET["vote_type"];
    $user_id = $_SESSION["user_id"];
    $comment_id = $_GET["comment_id"];

    $voted = did_vote($user_id, $comment_id);
    $voted_type = did_vote_type($user_id, $comment_id, $type);
    $result = false;

    //if already voted and the vote was from that type, remove that vote
    if ($voted && $voted_type) {
        $result = remove_votes($user_id, $comment_id);
    } else {
        //remove downvote if now upvoting for example
        if(!$voted_type){
            remove_votes($user_id, $comment_id);
        }
        $result = vote($user_id, $comment_id, $type);
    }
    if ($result) {
        echo "true";
//        echo "Success on comment " . $comment_id . " vote from user " . $user_id . ": " . bts($voted);
    } else {
        echo "error";
    }
}
