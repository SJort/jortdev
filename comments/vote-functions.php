<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/account/login-functions.php";

function get_vote_count($comment_id, $up)
{

    $sql = "SELECT COUNT(comment_id) AS total FROM comment_link WHERE comment_id=:id AND link_type=:type;";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":id", $comment_id, PDO::PARAM_STR);
        $type = $up ? "1" : "2";
        $statement->bindParam(":type", $type, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                $row = $statement->fetch();
                return $row["total"];
            }
        }
    }
    return -1; /*not 0 to indicate broken code*/
}


function did_vote($user_id, $comment_id)
{
    $sql = "SELECT COUNT(*) AS total FROM comment_link WHERE user_id=:user_id AND comment_id=:comment_id;";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $statement->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                $row = $statement->fetch();
                $total = $row["total"];
                if ($total === "0") {
                    return false;
                }
                return true;
            }
        }
    }
    echo_error("Database failed.");
    return true;
}

function did_vote_type($user_id, $comment_id, $comment_type)
{

    $sql = "SELECT COUNT(*) AS total FROM comment_link 
        WHERE user_id=:user_id AND comment_id=:comment_id AND link_type=:type";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $statement->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);
        $statement->bindParam(":type", $comment_type, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                $row = $statement->fetch();
                $total = $row["total"];
                if ($total === "0") {
                    return false;
                }
                return true;
            }
        }
    }
    echo_error("Database failed.");
    return true;
}

/**
 * Deletes all votes from a comment from a user.
 * Useful for case when downvoting an upvoted comment.
 * @param $user_id String
 * @param $comment_id String
 * @return bool True if successfully executed.
 */
function remove_votes($user_id, $comment_id)
{
    $sql = "DELETE FROM comment_link WHERE user_id=:user_id AND comment_id=:comment_id";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $statement->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);
        if ($statement->execute()) {
            return true;
        }
    }
    return false;

}

function vote($user_id, $comment_id, $vote_type)
{
    $sql = "INSERT INTO comment_link (user_id, comment_id, link_type) VALUES (:user_id, :comment_id, :type);";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $statement->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);
        $statement->bindParam(":type", $vote_type, PDO::PARAM_STR);
        if ($statement->execute()) {
            return true;
        }
    }
    return false;
}

//todo
function log_vote($user_id, $comment_id, $vote_type)
{
    $sql = "INSERT INTO log (user_id, comment_id, link_type) VALUES (:user_id, :comment_id, :type);";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $statement->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);
        $statement->bindParam(":type", $vote_type, PDO::PARAM_STR);
        if ($statement->execute()) {
            return true;
        }
    }
    return false;
}
