<?php
function echo_long()
{
    $result = "";
    for ($i = 0; $i < 40; $i++) {
        $result .= "LongLine" . $i;
    }
    echo $result;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Comments</title>
    <style>
        * {
            border: 1px solid grey;
        }

        html, body {
            margin: 0;
        }

        .center-parent {
            display: grid;
            place-items: center;
            height: 100vh;
            padding: 0px 10px 0px 10px; /*prevent text to touch border on smaller screens*/
        }

        .center-horizontal-parent {
            display: grid;
            place-items: start center;
        }

        .comment-root {
            display: flex;
            flex-direction: column;

        }

        .comment-parent {
            padding-left: 20px;
        }

        .comment-header {
            display: flex;
            flex-direction: row;
        }

        .comment-content{
            word-break: break-word;
        }

        .comment-footer {
            display: flex;
        }
    </style>
</head>

<body>
<div class="center-horizontal-parent">
    <div class="comment-root">
        <div class="comment-parent">
            <div class="comment-header">
                <div class="comment-header-item">
                    Name
                </div>
                <div class="comment-header-item">
                    Date
                </div>
            </div>
            <div class="comment-body">
                <div class="comment-content">
                    Content
                </div>
            </div>
            <div class="comment-footer">
                <div class="comment-footer-item">
                    Like
                </div>

                <div class="comment-footer-item">
                    Dislike
                </div>

                <div class="comment-footer-item">
                    Reply
                </div>
            </div>
            <div class="comment-replies">
                <div class="comment-reply-item">
                    <div class="comment-parent">
                        <div class="comment-header">
                            <div class="comment-header-item">
                                Name
                            </div>
                            <div class="comment-header-item">
                                Date
                            </div>
                        </div>
                        <div class="comment-body">
                            <div class="comment-content">
                                Content
                            </div>
                        </div>
                        <div class="comment-footer">
                            <div class="comment-footer-item">
                                Like
                            </div>

                            <div class="comment-footer-item">
                                Dislike
                            </div>

                            <div class="comment-footer-item">
                                Reply
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="comment-replies">
                <div class="comment-reply-item">
                    <div class="comment-parent">
                        <div class="comment-header">
                            <div class="comment-header-item">
                                Name

                            </div>
                            <div class="comment-header-item">
                                Date
                            </div>
                        </div>
                        <div class="comment-body">
                            <div class="comment-content">
                                <?php echo_long() ?>
                            </div>
                        </div>
                        <div class="comment-footer">
                            <div class="comment-footer-item">
                                Like
                            </div>

                            <div class="comment-footer-item">
                                Dislike
                            </div>

                            <div class="comment-footer-item">
                                Reply
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="comment-parent">
            <div class="comment-header">
                <div class="comment-header-item">
                    Name
                </div>
                <div class="comment-header-item">
                    Date
                </div>
            </div>
            <div class="comment-body">
                <div class="comment-content">
                    <?php echo_long() ?>
                </div>
            </div>
            <div class="comment-footer">
                <div class="comment-footer-item">
                    Like
                </div>

                <div class="comment-footer-item">
                    Dislike
                </div>

                <div class="comment-footer-item">
                    Reply
                </div>
            </div>

        </div>
        <div class="comment-parent">
            <div class="comment-header">
                <div class="comment-header-item">
                    Name

                </div>
                <div class="comment-header-item">
                    Date
                </div>
            </div>
            <div class="comment-body">
                <div class="comment-content">
                    Content
                </div>
            </div>
            <div class="comment-footer">
                <div class="comment-footer-item">
                    Like
                </div>

                <div class="comment-footer-item">
                    Dislike
                </div>

                <div class="comment-footer-item">
                    Reply
                </div>
            </div>

        </div>

    </div>
</div>

</body>
</html>
