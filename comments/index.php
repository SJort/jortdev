<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/comments/vote-functions.php";

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FORM SUBMIT FUNCTIONS
// Define variables and initialize with empty values
$comment = "";
$comment_error = "";
$comment_id = "";

//this gets triggered when submit is pressed
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!is_logged_in()) {
        $comment_error = "You need to be logged in to post.";
    }
    $comment = $_POST["comment-field"];
    if (strlen($comment) > 255) {
        $comment_error = "Comment too long (" . strlen($comment) . "/255).";
    }

    $comment_id = $_POST["comment_id"];
    if (empty($comment)) {
        $comment_error = "Comment is empty.";
    }
//    if($comment === "error"){
//        $comment_error = "Error test success";
//    }

    if (empty($comment_error)) {
        $user_id = $_SESSION["user_id"];


        $sql = "INSERT INTO comment (user_id, content, parent_comment_id) VALUES (:user_id, :content, :comment_id);";

        global $pdo;
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $stmt->bindParam(":content", $comment, PDO::PARAM_STR);

            //-1 is for parent comment form, so no reply to another comment
            if ($comment_id === "-1") {
                $comment_id = NULL;
            }
            $stmt->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);

            if ($stmt->execute()) {
//                $comment_err = "Comment posted!";
                //redirect to same page to avoid form page resubmission error
                header("Location: " . $_SERVER["PHP_SELF"]);
            } else {
                $comment_err = "Failed to post comment.";
            }
        }
        unset($stmt);
    }
}

function echo_long()
{
    $result = "";
    for ($i = 0; $i < 10; $i++) {
        $result .= "LongLine" . $i;
    }
    echo $result;
}

function echo_reply_button($pid)
{
    global $comment_id;
    $image_src = "/images/" . ($pid === $comment_id ? "close" : "reply") . ".svg";
    echo "<input type='image' src='" . $image_src . "' comment=" . $pid . " id=reply-" . $pid . " 
    onclick='toggle(this)' " . " alt='Reply button'/>";
}

function echo_reply_form($id, $pid, $username, $show, $is_for_child)
{
    global $comment_id, $comment_error, $comment; //these are set in the POST thing

    //local variables, otherwise globals get overwritten at the else
    $comment_text = "";
    $comment_err = "";

    //if the send comment is this comment, show the error and comment text
    if ($comment_id === $id) {
        $comment_err = $comment_error;
        $comment_text = $comment;
        $show = true;
    }


    echo "<form action='" . htmlspecialchars($_SERVER["PHP_SELF"]) . "' method='post' 
    comment=" . $id . " id=form-" . $id . " style='display:" . ($show ? "block" : "none") . "'>";
    echo "<div class='form-group " . (!empty($comment_err) ? "has-error" : "") . "'>";
    echo "<label>";
    if ($is_for_child) {
        $at_username = "@" . $username. " ";
//        if string not starts with @username (seems not needed as immer html does not actually update on type
        if (strncmp($comment_text, $at_username, strlen($at_username)) !== 0) {
            $comment_text = $at_username . $comment_text;
        }
    }
    echo "<textarea name='comment-field' id=comment_field-" . $id . " class='input'  rows='4' cols='40'>$comment_text</textarea>";
    echo "</label>";

    echo "<input type='hidden' name='comment_id' value=" . $pid . " />";
    echo "<br>";
    echo "<span class='help-block'>" . $comment_err . "</span>";
    echo "</div>";

    echo "<div class='form-group'>";
    $submit_text = "Submit " . ($username === "-1" ? "comment" : "reply to " . $username);

    echo "<input type='submit' class='input' value='" . $submit_text . "'>";
    echo "</div>";
    echo "</form>";
}

function did_user_vote($comment_id, $type)
{
    session_go();
    if (!is_logged_in()) {
        return false;
    }
    $user_id = $_SESSION["user_id"];
    $sql = "SELECT COUNT(*) AS total FROM comment_link 
        WHERE comment_id=:comment_id AND link_type=:type AND user_id=:user_id;";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $statement->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);
        $statement->bindParam(":type", $type, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                $row = $statement->fetch();
                $total = $row["total"];
                if ($total === "0") {
                    return false;
                }
                return true;
            }
        }
    }
    return false;
}

function echo_votes($comment_id, $up)
{
//    vague idea: adjust background color according to votes

    $up_string = $up ? "up" : "down"; //for the ids and stuff: upvoteButton-1
    $vote_code = $up ? "1" : "2"; //in the query, 1 means upvote and 2 downvote
    $image_src = "/images/" . $up_string . "vote" . (did_user_vote($comment_id, $vote_code) ? "d" : "") . ".svg";

    echo "<input type='image' src='" . $image_src . "' comment=" . $comment_id . " id=" . $up_string . "vote-" . $comment_id . " 
    onclick='vote(this, " . bts($up) . ")' " . " alt='" . $up_string . "vote button'/>";

    echo "(<span comment=" . $comment_id . " id=" . ($up ? "up" : "down") . "votecount-" . $comment_id . ">";
    echo get_vote_count($comment_id, $up);
    echo "</span>)";
}

/**
 * @param $pid String The ID of the comment.
 * @return bool True if the comment with the ID has replies.
 */
function has_children($pid)
{
    $sql = "SELECT comment.parent_comment_id FROM comment WHERE parent_comment_id=:pid";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":pid", $pid, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                return true;
            }
        }
    }
    return false;
}

function echo_comment($row, $pid, $is_child)
{
    $id = $row["id"];

    $url = "/user/" . $row["username"];
    $name = "<a class='link' href='". $url. "'>". $row["username"]. "</a>";
    $date = $row["creation_time"];
    $content = $row["content"];

    echo "<div class='comment-" . ($is_child ? "child" : "parent") . "'>";
    echo "<div class='comment-header'>";
    echo "<div class='comment-header-item'>";
    echo $name;
    echo "</div>";
    echo "<div class='comment-header-item'>";
    echo $date;
    echo "</div>";
    echo "</div>";

    echo "<div class='comment-body'>";
    echo "<div class='comment-body-item'>";
    echo htmlspecialchars($content);
    echo "</div>";
    echo "</div>";

    echo "<div class='comment-footer'>";
    echo "<div class='comment-footer-item'>";
    echo_votes($id, true);
    echo "</div>";
    echo "<div class='comment-footer-item'>";
    echo_votes($id, false);
    echo "</div>";
    echo "<div class='comment-footer-item'>";
    echo_reply_button($id);
    echo "</div>";
    echo "</div>";

    echo "<div class='comment-reply-form'>";

    //if its a child, give the id of the parent id mee, so the reply is submitted to the parent and not the child
    if($is_child){
        echo_reply_form($id, $pid, $row["username"], false, $is_child);
    }
    else{
        echo_reply_form($id, $id, $row["username"], false, $is_child);
    }
    echo "</div>";


    echo "<div class='comment-replies'>";
    if (!$is_child) {
        //merges the ids of comment with the usernames from user
        $sql = "SELECT user.username, comment.content, comment.id, comment.parent_comment_id, DATE_FORMAT(comment.creation_time, '%H:%i - %e/%m/%y') as creation_time
                    FROM comment INNER JOIN user ON user.id=comment.user_id where parent_comment_id=:pid 
                    ORDER BY comment.creation_time ASC;";

        global $pdo;
        if ($statement = $pdo->prepare($sql)) {
            $statement->bindParam(":pid", $id, PDO::PARAM_STR);
            if ($statement->execute()) {
                if ($statement->rowCount() > 0) {
                    while ($row = $statement->fetch()) {
                        echo_comment($row, $id, true);
                    }
                }
            }
        }
    }
    echo "</div>";
    echo "</div>";
}

function echo_comments()
{
    //merges the ids of comment with the usernames from user
    $sql = "SELECT user.username, comment.content, comment.id, comment.parent_comment_id, DATE_FORMAT(comment.creation_time, '%H:%i - %e/%m/%y') as creation_time
        FROM comment INNER JOIN user ON user.id=comment.user_id WHERE comment.parent_comment_id IS NULL
        ORDER BY comment.creation_time DESC;";


    global $pdo;
    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                while ($row = $stmt->fetch()) {
                    echo_comment($row, $row["id"], false);
                }
            }
        }
        unset($stmt);
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Comments</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link rel="stylesheet" href="/css/comment-styles.css">
    <script src="/js/base-devel.js"></script>
    <script src="/comments/comments.js"></script>
</head>

<body>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . "/account/profile-box.php"; ?>
<div class="center-horizontal-parent">
    <div>
        <h2>
            Comments
        </h2>
        <?php

        echo_reply_form("-1", "-1", "-1", true, false);
        echo "<div class='comment-root'>";
        echo_comments();
        echo "</div>";

        ?>
    </div>

</div>

</body>
</html>
