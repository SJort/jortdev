<!DOCTYPE html>
<html lang="en">

<script>
    <?php
    $folders_path = $_SERVER["DOCUMENT_ROOT"] . "/images";
    $folders = array_diff(scandir($folders_path), array('.', '..'));
    $selected_folder = $_GET["folder"];

    $json_main = new \stdClass();
    foreach ($folders as $folder) {
        $image_folder_path = $folders_path . "/" . $folder;
        if (!is_dir($image_folder_path)) {
            continue;
        }
        if ($folder != $selected_folder) {
            continue;
        }

        $json_main->{$folder} = array();

        $image_folder = array_diff(scandir($image_folder_path), array('.', '..'));
        foreach ($image_folder as $image_filename) {
            $image_data = new \stdClass();
            $image_data->{"filename"} = $image_filename;
            if (!preg_match("/\.(?:jpg|png|gif)$/", $image_filename)) {
                continue;
            }
            list($width, $height) = getimagesize($image_folder_path . "/" . $image_filename);
            $image_data->{"width"} = $width;
            $image_data->{"height"} = $height;
            array_push($json_main->{$folder}, $image_data);
        }
    }
    $json = json_encode($json_main);
    echo "let json = " . $json . ";\n";
    echo "let folder = '" . $selected_folder . "';";
    ?>
</script>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Jort - <?php echo $selected_folder?></title>
    <!--    photoswipe library: https://photoswipe.com -->
    <link rel="stylesheet" href="/libraries/gallery/photoswipe.css">
    <link rel="stylesheet" href="/libraries/gallery/default-skin/default-skin.css">
    <script src="/libraries/gallery/photoswipe.min.js"></script>
    <script src="/libraries/gallery/photoswipe-ui-default.min.js"></script>

    <script src="/libraries/js.cookie.min.js"></script>
    <script src="/folderviewer/thread.js"></script>
    <script src="/js/base-devel.js"></script>


    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
</head>
<body>

<div class="center-parent">
    <div class="center-child">
        <h1>Back</h1>
    </div>
</div>

<!--this with comments @ documentation-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true" id="viewer-root">
    <div class="pswp__bg"></div>

    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
