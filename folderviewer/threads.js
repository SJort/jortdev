document.addEventListener('DOMContentLoaded', function () {
    onLoad();
    console.log("heyy");
}, false);

function onLoad(){
    console.log("Hey lol");
    // appendSeenCheckboxes();
}

//TODO: clean this spaghetti, maybe load checkboxes PHP?
function appendSeenCheckboxes(){
    let table = document.getElementById("table");
    for (var i = 0, row; row = table.rows[i]; i++) {
        console.log("Row: " + row.innerHTML);

        if(row.innerHTML.includes("<a hre")){
            console.log("Appending checkbox...");
            let col = row.cells[0];
            console.log("Col: " + col.innerText);
            let folder = row.cells[0].innerText;
            let currentRow = row;
            for (let column = 0; column < 3; column++) {
                currentRow.insertCell();
            }

            //presence checkbox
            //https://hunzaboy.github.io/CSS-Checkbox-Library/#
            let checkbox = document.createElement('input');
            checkbox.setAttribute("type", "checkbox");
            checkbox.setAttribute("id", folder);
            checkbox.setAttribute("value", "0");
            checkbox.setAttribute("checked", "");
            checkbox.setAttribute("name", "ckbx-square-1");

            let checkboxLabel = document.createElement("label");
            checkboxLabel.setAttribute("for", folder);

            let checkboxContainer = document.createElement("div");
            checkboxContainer.setAttribute("class", "ckbx-style-8 ckbx-medium");
            checkboxContainer.appendChild(checkbox);
            checkboxContainer.appendChild(checkboxLabel);


            //modified indicator, always there because otherwise when inserted on interaction, the table shifts
            let modifiedCell = currentRow.cells[3];
            modifiedCell.innerText = "modified";
            modifiedCell.setAttribute("style", "opacity: 0");

            // save initial value so we can determine if the value is altered
            if (Cookies.get("read") && Cookies.get("read").includes(folder)) {
                checkbox.setAttribute("originalValue", 1);
            } else {
                checkbox.setAttribute("originalValue", 0);
            }

            //set checkbox checked on basis of the saved initial value
            checkbox.checked = checkbox.getAttribute("originalValue") === "1";

            checkbox.addEventListener('change', function () {
                //handle the modified indicator:

                //if the original value matches the current value, hide the modified indicator
                if (checkbox.getAttribute("originalValue") === "1" && this.checked || checkbox.getAttribute("originalValue") === "0" && !this.checked) {
                    //toggle visibility because its always there
                    modifiedCell.setAttribute("style", "opacity: 0");
                }
                //else show the modified indicator
                else {
                    modifiedCell.setAttribute("style", "opacity: 1;");
                    //bootstrap theme color
                    modifiedCell.className = "text-warning";
                }

                //update cookie with read threads
                updateCookie(folder, this.checked);
            });

            //add the created checkbox to the row
            currentRow.cells[2].appendChild(checkboxContainer);
        }
    }
}

function updateCookie(folder, read) {
    htmlLog("Updating cookie: " + folder + " read: " + read);
    let selected = Cookies.get("read");
    if (!selected) {
        selected = "";
    }
    // log("Selected: " + selected);
    if (selected.includes(folder) === read) {
        // log("Folder already properly in cookie");
        return;
    }
    if (read) {
        selected += folder + ", ";
    } else {
        selected = selected.replace(folder + ", ", "");
    }
    // log("Selected after: " + selected);
    Cookies.set("read", selected, {expires: 365});
}

