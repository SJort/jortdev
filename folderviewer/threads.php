<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Jort - Select thread</title>

    <!--    photoswipe library: https://photoswipe.com -->
    <link rel="stylesheet" href="/libraries/gallery/photoswipe.css">
    <link rel="stylesheet" href="/libraries/gallery/default-skin/default-skin.css">
    <script src="/libraries/gallery/photoswipe.min.js"></script>
    <script src="/libraries/gallery/photoswipe-ui-default.min.js"></script>

    <script src="/js/base-devel.js"></script>
    <script src="/js/js.cookie.min.js"></script>
    <script src="/folderviewer/threads.js"></script>

    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/libraries/checkboxes.min.css">
</head>
<body>

<div class="center-horizontal-parent">
    <div class="center-child">
        <h1>Select thread</h1>
        Below are all the images from
        <br>the threads on <a href="https://www.4chan.org/b" class="link">4chan.org/b</a>
        <br>containing 'YLYL' in the title
        <br>since 9 januari 2020.

        <div id="threads"></div>
        <table id="table">
            <tr>
                <th>Thread</th>
                <th>Images</th>
            </tr>
            <?php
            $folders_path = $_SERVER["DOCUMENT_ROOT"] . "/images";
            $folders = array_diff(scandir($folders_path), array('.', '..'));

            $page = $_GET["page"];
            $folders_per_page = 20;
            //i honestly figured these equations out by trial and error
            $start_count = $folders_per_page * $page - $folders_per_page + 1;
            $end_count = $start_count + $folders_per_page - 1;
//            echo "<br>page: " . $page . ", start: " . $start_count . ", end: ", $end_count . "<br>";


            $link_base = "/threads/";

            $count = 0;
            $actual_folder_count = 0;
            foreach ($folders as $folder) {
                $image_folder_path = $folders_path . "/" . $folder;
                if (!is_dir($image_folder_path)) {
                    continue;
                }
                $actual_folder_count++;

                $count++;
                if ($count < $start_count || $count > $end_count) {
                    continue;
                }

                $folder_items = array_diff(scandir($image_folder_path), array('.', '..'));
                $amount_of_items = sizeof($folder_items);
                echo "\n<tr><td><a href='" . $link_base . $folder . "' target='_blank' class='link' onclick=\"updateCookie('" . $folder . "', true)\">" . $folder . "</a></td><td>" . $amount_of_items . "</td></tr>";
            }

            //determine which pages to show in footer
            $amount_of_pages = intval($actual_folder_count / $folders_per_page);
            $page_delta = 5;
            $start_page_count = $page - $page_delta;
            $end_page_count = $page + $page_delta + 1;
            if ($start_page_count < 1) {
                $start_page_count = 1;
            }
            if ($end_page_count > $amount_of_pages) {
                $end_page_count = $amount_of_pages;
            }
            //            echo "<br>Start: " . $start_page_count . ", end: " . $end_page_count . ", pages: " . $amount_of_pages . "<br>";
            ?>
        </table>

        <div id="pages">
            <?php
            for ($i = $start_page_count; $i < $end_page_count; $i++) {
                if($i == $page){
                    echo "<a href='/threads/page-" . $i . "' class='clicked-link'>  " . $i . "  </a>";
                }
                else{
                    echo "<a href='/threads/page-" . $i . "' class='link'>  " . $i . "  </a>";
                }
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>
