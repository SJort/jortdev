document.addEventListener('DOMContentLoaded', function () {
    onLoad();
}, false);

/*
items is een array van objecten waar elk object wordt een dropdown menu wordt
de root is de naam van de dropdown, en in images gaan de images die in die dropdown moeten
images is een (weer) een array van objecten, waar elk object een title en een url heeft
laat title leeg of weg om bestandnaam als titel te gebruiken
*/
let items = [
    {
        root: "All",
        images: [
            {
                title: "9 februari 2020",
                url: "/images/dropdown/all/all1.png",
            },
            {
                title: "16 februari 2020",
                url: "/images/dropdown/all/all2.png",
            },
        ]
    },
    {
        root: "Neofetch",
        images: [
            {
                url: "/images/dropdown/neofetch/neo1.jpg",
            },
            {
                url: "/images/dropdown/neofetch/neo2.png",
            },
            {
                url: "/images/dropdown/neofetch/neo3.png",
            },
        ]
    },
    {
        root: "Parsed",
        images: [
            {
                url: "/images/dropdown/parsed/parsed1.png",
            },
        ]
    },
];

let regexFilename = new RegExp(".*\\/(.*)\\.");

function onLoad() {
    loadImage(items[0].images[0]); //load the first item
    items.forEach(function (item) {
        createDropdown(item);
    });
}

function getImageTitle(imageObject) {
    let title = imageObject.title;
    if (!title) {
        title = regexFilename.exec(imageObject.url); //if no title defined use url
        if (title) {
            title = title[1];
        } else { //fallback if regex is not working
            title = imageObject;
        }
    }
    return title;
}

function loadImage(imageObject) {
    let imageHolder = document.getElementById("imageHolder");
    let image = document.getElementById("image");
    image.setAttribute("src", imageObject.url);
    let title = document.getElementById("title");
    title.innerHTML = getImageTitle(imageObject);
    imageHolder.innerHTML = "";
    imageHolder.appendChild(image);
    imageHolder.appendChild(title);
}

function createDropdown(item) {
    let table = document.getElementById("table");
    let row = table.insertRow();
    let cell = row.insertCell();

    let type = document.createElement("div");
    type.setAttribute("id", "type");
    type.setAttribute("class", "dropdown");
    let typeButton = document.createElement("button");
    let id = item.root;
    typeButton.setAttribute("class", "btn btn-secondary dropdown-toggle");
    typeButton.setAttribute("type", "button");
    typeButton.setAttribute("id", id);
    typeButton.setAttribute("data-toggle", "dropdown");
    typeButton.setAttribute("aria-haspopup", "true");
    typeButton.setAttribute("aria-expanded", "false");
    typeButton.innerHTML = item.root;
    let optionsRoot = document.createElement("div");
    optionsRoot.setAttribute("class", "dropdown-menu");
    optionsRoot.setAttribute("aria-labelledby", id);
    item.images.forEach(function (imageObject) {
        let option = document.createElement("a");
        option.setAttribute("class", "dropdown-item");
        option.setAttribute("href", "#");
        option.innerHTML = getImageTitle(imageObject);
        option.addEventListener("click", function () {
            loadImage(imageObject);
        });
        optionsRoot.appendChild(option);
        type.appendChild(typeButton);
        type.appendChild(optionsRoot);
        cell.appendChild(type);
    });
}
