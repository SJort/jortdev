<!DOCTYPE html>
<html lang="en">
<head>
    <title>Images</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <script src="script.js"></script>
</head>
<body>
<div>
    <div style="float:left">
        <table class="table">
            <tbody id="table">
            </tbody>
        </table>
    </div>
    <div id="imageHolder" style="float: left; padding: 0.5vw">
        <img id="image" src="https://imgur.com/EH98O9u.png" style="max-width: 100%; max-height: 90vh;">
        <h4 id="title" class="display-4" style="max-height: 9vh">Title</h4>
    </div>
</div>

<script src="libraries/jquery-3.4.1.min.js"></script>
<script src="libraries/bootstrap/js/bootstrap.bundle.js"></script>
</body>
</html>