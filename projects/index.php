<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Projects</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

</head>
<body>
<div class="center-parent">
    <div class="center-child">
        <h1>Projects</h1>
        <ul>
            <li>
                <a class="link" href="/projects/countdown/">Calendar</a>
                <br>
                Global calendar made for colleague for &euro;30.
            </li>
            <li>
                <a class="link" href="/projects/dropdown/">Dropdown</a>
                <br>
                Predefined image dropdown made for colleague for &euro;20.
            </li>
        </ul>
    </div>
</div>

</body>
</html>
