document.addEventListener('DOMContentLoaded', function () {
    onLoad();
}, false);

const amountOfOptions = 4;

function handlePasswordInput(){
    let password = prompt("Voer wachtwoord in");
    if(password !== null){
        //http://localhost/test/testsessionserver?request=setPassword&password=hoii
        let request = "server.php?request=setPassword&password=" + password;
        console.log("Request: " + request);

        fetch(request)
            .then((response) => {
                // return response.json(); //must be separate promise because .json is also a promise..?
                return response.json();
            })
            .then((response) => {
                console.log("Request result: " + JSON.stringify(response));
                if(response["response"].includes("granted")){
                    alert("Access granted.");
                }
                else {
                    alert("Access denied.");
                }
            })
            .catch(err => {
                console.log("Error with request:");
                console.log(err);
            });
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function loadCalendar(response) {
    console.log("Loading calendar with " + JSON.stringify(response));

    let optics = [];

    response.forEach(function (item) {
        console.log("Iterating: " + JSON.stringify(item));
        let optic = {};
        optic.date = new Date(item.date);
        optic.type = parseInt(item.type);
        optic.title = item.title;
        optics.push(optic);
    });
    let $calendar = document.getElementById("calendar");
    let currentYear = new Date().getFullYear();
    var calendarize = new Calendarize();
    calendarize.setOptics(optics);
    calendarize.buildYearCalendar($calendar, currentYear, optics);
}

function onLoad() {
    let request = "server.php?request=getEvents";
    console.log("Request: " + request);

    fetch(request)
        .then((response) => {
            // return response.json(); //must be separate promise because .json is also a promise..?
            return response.json();
        })
        .then((response) => {
            handleResponse(response);
        })
        .catch(err => {
            console.log("Error in response, using sample data");
            let response = '[{"title":"Test data: someting wong","date":"2020-05-26","type":"1"},{"title":"DB connection oopsie woopsied","date":"2020-05-27","type":"2"},{"title":"TheseAreSampleData","date":"2020-02-26","type":"0"}]';
            response = JSON.parse(response);
            handleResponse(response);
        });

}

function typeToString(type) {
    switch (type) {
        case 1: {
            return "Evenement";
        }
        case 2: {
            return "Photoshoot";
        }
        case 3: {
            return "Vakantie";
        }
        default: {
            return "Overig";
        }
    }
}

function typeToStringAndColor(type) {
    type = parseInt(type);
    let string = typeToString(type);
    let color;
    switch (type) {
        case 1: {
            color = "green";
            break;
        }
        case 2: {
            color = "red";
            break;
        }
        case 3: {
            color = "yellow";
            break;
        }
        default: {
            color = "blue";
            break;
        }
    }
    let result = {};
    result.string = string;
    result.color = color;
    return result;
}

function handleResponse(response) {
    console.log("Working with response: " + JSON.stringify(response));
    clearTable();
    loadTable(response);
    clearCalendar();
    loadCalendar(response);
}

function addItem(event) {
    console.log("Adding event: " + JSON.stringify(event));
    // request=addEvent&title=haha&date=hoi&type=3

    let request = "server.php?request=addEvent&title=" + event["title"] + "&date=" + event["date"] + "&type=" + event["type"];
    console.log("Request: " + request);

    fetch(request)
        .then((response) => {
            // return response.json(); //must be separate promise because .json is also a promise..?
            return response.json();
        })
        .then((response) => {
            let res = JSON.stringify(response);
            if(res.includes("denied")){
                alert("Access denied.");
            }
            console.log("Request result: " + J);

        })
        .catch(err => {
            console.log("Error with request:");
            console.log(err);
        });
    //otherwise getEvents is not updated yet
    sleep(200);
}

function removeItem(event) {
    console.log("Removing event: " + JSON.stringify(event));
    // request=addEvent&title=haha&date=hoi&type=3

    let request = "server.php?request=removeEvent&title=" + event["title"] + "&date=" + event["date"] + "&type=" + event["type"];
    console.log("Request: " + request);

    fetch(request)
        .then((response) => {
            // return response.json(); //must be separate promise because .json is also a promise..?
            return response.json();
        })
        .then((response) => {
            let res = JSON.stringify(response);
            if(res.includes("denied")){
                alert("Access denied.");
            }
            console.log("Request result: " + JSON.stringify(response));
        })
        .catch(err => {
            console.log("Error with request:");
            console.log(err);
        });

    sleep(200);

}

function clearTable() {
    let table = document.getElementById("table");
    let new_tbody = document.createElement('tbody');
    new_tbody.id = "table";
    table.parentNode.replaceChild(new_tbody, table);
}

function clearCalendar() {
    let calendar = document.getElementById("calendar");
    calendar.innerHTML = "";
}

function loadTable(response) {
    console.log("Loading table with " + JSON.stringify(response));
    let amountOfColumns = 5;

    response.forEach(function (item) {
        console.log("Iterating: " + JSON.stringify(item));
        if (item === "") {
            return;
        }
        // let itemObject = JSON.parse(item);
        let itemObject = item;

        let currentDate = new Date();
        let dateObject = new Date(itemObject["date"]);
        const diffTime = dateObject - currentDate;
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        if (diffDays < 0) {
            console.log("Not showing past item: " + JSON.stringify(itemObject));
            return;
        }

        let currentRow = table.insertRow();
        for (let column = 0; column < amountOfColumns; column++) {
            currentRow.insertCell();
        }

        let title = document.createElement("div");
        title.innerHTML = itemObject["title"];
        currentRow.cells[0].appendChild(title);

        let type = document.createElement("div");
        let ddd = typeToStringAndColor(itemObject["type"]);
        type.innerHTML = ddd.string;
        type.setAttribute("style", "border-right: 5px solid " + ddd.color);
        currentRow.cells[1].appendChild(type);

        let date = document.createElement("div");
        let dateString = new Date(itemObject["date"]).toLocaleDateString('en-GB', {timeZone: 'UTC'}).split(",")[0];
        date.innerHTML = dateString;
        currentRow.cells[2].appendChild(date);

        let left = document.createElement("div");
        let leftValue = diffDays + " days";
        if (leftValue < 0) {
            console.log("Not showing past item: " + JSON.stringify(itemObject));
            return;
        }
        left.innerHTML = leftValue;
        currentRow.cells[3].appendChild(left);

        let button = document.createElement("button");
        // button.setAttribute("type", "button");
        button.setAttribute("class", "btn btn-danger");
        button.innerHTML = "Verwijderen";
        button.addEventListener("click", function () {
            removeItem(itemObject);
            onLoad();
        });
        currentRow.cells[4].appendChild(button);
    });

    let currentRow = table.insertRow();

    //create empty cells in the just inserted row
    for (let column = 0; column < amountOfColumns; column++) {
        currentRow.insertCell();
    }

    let title = document.createElement("input");
    title.setAttribute("type", "text");
    title.setAttribute("class", "form-control");
    title.setAttribute("placeholder", "naam");
    title.setAttribute("id", "title");
    currentRow.cells[0].appendChild(title);

    // let type = document.createElement("input");
    // type.setAttribute("type", "number");
    // type.setAttribute("class", "form-control");
    // type.setAttribute("min", "1");
    // type.setAttribute("max", "3");
    // type.setAttribute("id", "type");
    let type = document.createElement("div");
    type.setAttribute("id", "type");
    type.setAttribute("class", "dropdown");
    let typeButton = document.createElement("button");
    let id = "typeInput";
    typeButton.setAttribute("class", "btn btn-secondary dropdown-toggle");
    typeButton.setAttribute("type", "button");
    typeButton.setAttribute("id", id);
    typeButton.setAttribute("data-toggle", "dropdown");
    typeButton.setAttribute("aria-haspopup", "true");
    typeButton.setAttribute("aria-expanded", "false");
    typeButton.innerHTML = "Type";
    let optionsRoot = document.createElement("div");
    optionsRoot.setAttribute("class", "dropdown-menu");
    optionsRoot.setAttribute("aria-labelledby", id);
    for (let i = 0; i < amountOfOptions; i++) {
        let option = document.createElement("a");
        option.setAttribute("class", "dropdown-item");
        option.setAttribute("href", "#");
        option.innerHTML = typeToString(i);
        option.addEventListener("click", function () {
            typeButton.innerHTML = option.innerHTML;
            typeButton.setAttribute("valueId", i);
        });
        optionsRoot.appendChild(option);
    }
    type.appendChild(typeButton);
    type.appendChild(optionsRoot);

    currentRow.cells[1].appendChild(type);

    let date = document.createElement("input");
    date.setAttribute("id", "date");
    date.setAttribute("class", "form-control");
    date.setAttribute("value", new Date().toISOString().split('T')[0]);
    // date.setAttribute("placeholder", "required");
    date.setAttribute("type", "date");
    currentRow.cells[2].appendChild(date);

// <button onclick="handlePasswordInput()" type="button" class="btn btn-primary">Login</button>
    let left = document.createElement("button");
    left.setAttribute("class", "btn btn-primary");
    left.innerHTML = "Login";
    left.addEventListener("click", handlePasswordInput);
    currentRow.cells[3].appendChild(left);

    let button = document.createElement("button");
    button.innerHTML = "Toevoegen";
    button.setAttribute("class", "btn btn-success");
    button.addEventListener("click", function () {
        console.log("clicked  to add: ");
        let newTitle = document.getElementById("title").value;
        let newDate = document.getElementById("date").value;
        let newType = document.getElementById(id).getAttribute("valueId");

        if (newTitle === "" || newDate === "") {
            console.log("skipping empty entry");
        }
        if (newType === "" || newType === null) {
            newType = "0";
        }
        console.log("Title: " + newTitle + ", date: " + newDate);
        let newItem = {};
        newItem.title = newTitle;
        newItem.date = newDate;
        newItem.type = newType;
        addItem(newItem);
        onLoad();
    });
    currentRow.cells[4].appendChild(button);
}
