<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">-->
    <meta name="viewport" content="width=100%"> <!--with above one table loads too much zoomed in-->
    <title>Calender</title>

    <!--    bootstrap-->
    <link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="../../libraries/calendar/calendarstyle.css">
    <script src="../../libraries/js.cookie.min.js"></script>
    <script type="text/javascript" src="../../libraries/calendar/calendarize.js"></script>
    <script src="scripts.js"></script>
<!--    <link rel="stylesheet" href="styles.css">-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
<!--    <link rel="shortcut icon" type="image/png" href="favicon.ico">-->

<body>
<div class="center">
<!--    <h1>Calendar</h1>-->

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th>Naam</th>
                <th>Type</th>
                <th>Datum</th>
                <th>Over</th>
                <th>Optie</th>
            </tr>
        </thead>
        <tbody id="table">

        </tbody>
    </table>
</div>
<div id="calendar"></div>


<!--these must be here at the end of body-->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
