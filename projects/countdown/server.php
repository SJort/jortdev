<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dsn = 'mysql:dbname=kevinbergenhen_website;host=localhost;port=3306;charset=utf8';

//SWITCH THESE FOR TESTING LOCAL
//$connection = new \PDO($dsn, "kevinbergenhen", "xJjKQm4fr9j1");
$connection = new \PDO($dsn, "root", "root");

// throw exceptions wanneer je een SQL error krijgt
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

if ($_GET["request"] == "test") {
    $response = new \stdClass();
    $response->{"data"} = "hoi haha";
    $json = json_encode($response);
    echo $json;
}


//http://localhost/test/testsessionserver?request=setPassword&password=hoii
if ($_GET["request"] == "setPassword" && ISSET($_GET["password"])) {
    $password = $_GET["password"];

    $query = "select * from users where password='" . $password . "';";

    $response = new \stdClass();
    $response->{"query"} = $query;

    $statement = $connection->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    if ($result == []){
        $response->{"response"} = "Access denied.";
        session_unset();
    }
    else {
        $response->{"response"} = "Access granted.";
        $_SESSION["password"] = $password;
    }
//    $response->{"results"} = $result;
    $json = json_encode($response);
    echo $json;
}

elseif ($_GET["request"] == "getEvents") {
    $query = $connection->prepare("select * from events ORDER BY date ASC;");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($result);
    echo $json;
}

elseif(!ISSET($_SESSION["password"])) {
    $response = new \stdClass();
    $response->{"response"} = "Access denied. Authenticate first.";
    $json = json_encode($response);

    echo $json;
    exit(1);
}

//request=addEvent&title=haha&date=hoi&type=3
if ($_GET["request"] == "addEvent" && ISSET($_GET["title"]) && ISSET($_GET["date"]) && ISSET($_GET["type"])) {
    /*
    INSERT INTO events (title, date, type) VALUES ("Comic Con", "2020-05-26", 1);
    INSERT INTO events (title, date, type) VALUES ("Photoshoot met Kerel", "2020-05-27", 2);
    INSERT INTO events (title, date, type) VALUES ("Vakantie", "2020-08-3", 2);
    */

    $title = $_GET["title"];
    $date = $_GET["date"];
    $type = $_GET["type"];

    $query = "INSERT INTO events (title, date, type) VALUES ('" . $title . "', '" . $date . "', '" . $type . "');";

    $response = new \stdClass();
    $response->{"response"} = "Success: Adding event with title=" . $title . ", date=" . $date . ", type=" . $type . " with: " . $query . ".";
    $json = json_encode($response);

    $statement = $connection->prepare($query);
    $statement->execute();

    echo $json;
}
//elseif ($_GET["request"] == "addEvent") {
//    $response = new \stdClass();
//    $response->{"response"} = "Error: variables not correctly set.";
//    $json = json_encode($response);
//    echo $json;
//}

if ($_GET["request"] == "removeEvent" && ISSET($_GET["title"]) && ISSET($_GET["date"]) && ISSET($_GET["type"])) {
//    INSERT INTO events (title, date, type) VALUES ("Comic Con", "2020-05-26", 1)

    $title = $_GET["title"];
    $date = $_GET["date"];
    $type = $_GET["type"];

    $query = "DELETE FROM events WHERE title='" . $title . "' AND date='" . $date . "' AND type='" . $type . "' LIMIT 1";

    $response = new \stdClass();
    $response->{"response"} = "Success: Removing event with title=" . $title . ", date=" . $date . ", type=" . $type . " with: " . $query . ".";
    $json = json_encode($response);

    $statement = $connection->prepare($query);
    $statement->execute();

    echo $json;
}
//elseif ($_GET["request"] == "addEvent") {
//    $response = new \stdClass();
//    $response->{"response"} = "Error: variables not correctly set.";
//    $json = json_encode($response);
//    echo $json;
//}

