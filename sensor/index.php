<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Acceleration test</title>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
</head>
<body>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . "/account/profile-box.php"; ?>
<div class="center-horizontal-parent">
    <div class="columns">
        <h2>Acceleration meter test</h2>
        <i>The sensors are only accessible on HTTPS connections.</i>
        <br>

        <h4>Current readings</h4>
        X-axis:
        <span id="accelX"></span>
        Y-axis:
        <span id="accelY"></span>
        Z-axis:
        <span id="accelZ"></span>

        <h4>Graph</h4>
        <div id="chartContainer" style="height: 370px; width:100%;"></div>

        <h2>
            Console
        </h2>
        <i>All console messages are also printed below.</i>
        <br>
        <div id="logBar">

        </div>

    </div>
</div>
</body>

<script src="/js/canvasjs.min.js"></script>
<script src="/js/base-devel.js"></script>
<script src="/sensor/sensor.js"></script>
</html>
