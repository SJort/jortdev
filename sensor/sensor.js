function setupSensor() {
    console.log("Setting up sensor.")
    try {
        acl = new Accelerometer({referenceFrame: "device", frequency: 60});
        acl.addEventListener("error", event => {
            // Handle runtime errors.
            if (event.error.name === "NotAllowedError") {
                // Branch to code for requesting permission.
                console.log("Sensor access not allowed by user.");
            } else if (event.error.name === "NotReadableError") {
                console.log("Cannot connect to the sensor.");
            }
        });
        acl.start();
    } catch (error) {
        // Handle construction errors.
        if (error.name === "SecurityError") {
            // See the note above about feature policy.
            console.log("Sensor construction was blocked by a feature policy.");
        } else if (error.name === "ReferenceError") {
            console.log("Sensor is not supported by the User Agent.");
        } else {
            throw error;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
overwriteConsole();
console.log("Init");
let acl = null;
let xVal = 0;

// dataPoints
let dpsX = [];
let dpsY = [];
let dpsZ = [];
setupSensor();

//setup listener
acl.addEventListener("reading", () => {
    let xContainer = document.getElementById("accelX");
    let yContainer = document.getElementById("accelY");
    let zContainer = document.getElementById("accelZ");
    xContainer.innerText = acl.x;
    yContainer.innerText = acl.y;
    zContainer.innerText = acl.z;
    xVal++;
    dpsX.push({
        x: xVal,
        y: acl.x
    });
    dpsY.push({
        x: xVal,
        y: acl.y
    });

    dpsZ.push({
        x: xVal,
        y: acl.z
    });

});

let chart = new CanvasJS.Chart("chartContainer", {
    // title: {
    //     text: "Sensor output"
    // },
    axisX: {
        title: "Time"
    },
    axisY: {
        title: "Sensor value"
    },
    data: [
        {
            type: "spline",
            dataPoints: dpsX,
            axisYType: "secondary",
            name: "X",
            showInLegend: true,
        },
        {
            type: "spline",
            dataPoints: dpsY,
            axisYType: "secondary",
            name: "Y",
            showInLegend: true,
        },
        {
            type: "spline",
            dataPoints: dpsZ,
            axisYType: "secondary",
            name: "Z",
            showInLegend: true,
        }
    ]
});

let updateInterval = 100;
let dataLength = 200; // number of dataPoints visible at any point

let updateChart = function () {
    if (dpsX.length > dataLength) {
        let toRemove = dpsX.length - dataLength;
        dpsX.splice(0, toRemove);
    }

    if (dpsY.length > dataLength) {
        let toRemove = dpsY.length - dataLength;
        dpsY.splice(0, toRemove);
    }

    if (dpsZ.length > dataLength) {
        let toRemove = dpsZ.length - dataLength;
        dpsZ.splice(0, toRemove);
    }

    chart.render();
};

updateChart(dataLength);
setInterval(function () {
    updateChart()
}, updateInterval);
console.log("Init done");





