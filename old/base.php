<?php
// Initialize the session
session_start();

$selected_user = "Anonymous";
if (isset($_GET["user"])) {
    $selected_user = $_GET["user"];
}

$user_exists = false;
$type = 0;
$is_admin = false;

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/config.php";

$sql = "SELECT * FROM users WHERE username = :username;";

if ($stmt = $pdo->prepare($sql)) {
    $stmt->bindParam(":username", $selected_user, PDO::PARAM_STR);

    if ($stmt->execute()) {
        if ($stmt->rowCount() == 1) {
            $user_exists = true;
            if ($row = $stmt->fetch()) {
                $type = $row["type"];
                if($type == ""){
                    $type = 0;
                }
                echo "privilege: ", $type;
            } else {
                setError("More than 1 user.");
            }
        } else {
            setError("Failed to execute query.");
        }
        unset($stmt);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title><?php echo $selected_user ?></title>
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="centered-flex-root-container">
<div class="centered-flex-item">
    <?php
    if (!$user_exists) {
        echo "<h1 class='display-1'>This user does not exist</h1>";
    } else {
        echo "<h1 class='display-1'>$selected_user</h1>";
    }
    if (isset($_SESSION["username"]) && $_SESSION["username"] == $selected_user) {
        include $_SERVER['DOCUMENT_ROOT'] . "/profile/my-profile.php";
    }
    ?>
</div>

<?php
include $_SERVER['DOCUMENT_ROOT'] . "/profile/profile-box.php";
include $_SERVER['DOCUMENT_ROOT'] . "/php/error-box.php";
?>

<script src="/libraries/jquery-3.4.1.min.js"></script>
<script src="/libraries/bootstrap/js/bootstrap.bundle.js"></script>
</body>
</html>
