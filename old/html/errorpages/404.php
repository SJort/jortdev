<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>404</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="center center-vertical">
<div class="center">
    <h1>404</h1>
    <br>
    <p style="font-size:10px;">that's a number</p>
</div>

</body>
</html>