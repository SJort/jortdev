document.addEventListener("DOMContentLoaded", function() {
    onStart();
});

function onStart(){
    log("Request from PHP: " + request);
    loadImages(request);
}

function handleImagesResponse(response) {
    let result = [];
    let folder = Object.keys(response)[0];
    logVar(folder);
    let images = response[folder];
    images.forEach(function(imageObject){
        let fileName = imageObject["filename"];
        let imagePath = "/threads/" + folder + "/" + fileName;
        let width = imageObject["width"];
        let height = imageObject["height"];
        let item = {
            src: imagePath,
            w: width,
            h: height,
            pid: fileName,

        };
        log("Pushing: " + JSON.stringify(item));
        result.push(item);
    });
    return result;
}

function loadImages(requestedFolder){
    log("Loading images");
    Cookies.set("selected", requestedFolder);
    let selected = Cookies.get("selected");
    let imagesRequest =  "/photoviewer/server.php?request=getImages&folder=" + selected;
    log("Request: " + imagesRequest);

    fetch(imagesRequest)
        .then((response) => {
            return response.json(); //must be separate promise because .json is also a promise..?
        })
        .then((response) => {
            return handleImagesResponse(response);
        })
        .then((response) => {
            loadGallery(response, request);
        })
		.catch(err => {
		    log(err);
		    log("Folder probably does not exist, redirecting...");
            window.location.replace("/threads");
        });
}

function loadGallery(items, folder){
    log("Loading gallery with " + items);
    let pswpElement = document.querySelectorAll('.pswp')[0];

    let cookieName = folder + "-index";
    let leftAt = Cookies.get(cookieName);
    if(!leftAt){
        leftAt = 0;
    }
    else{
        leftAt = parseInt(leftAt);
    }


    let options = {
        index: leftAt, //start where left off
        pinchToClose: false,
        closeOnScroll: false,
        // closeOnVerticalDrag: false,
        // escKey: false,
        history: false,
        galleryPIDs: true,
        galleryUID: folder,
        preload: [2, 5],
    };

    let pswp = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    pswp.listen('close', function() {
        log("Close detected, redirecting...");
        window.location.replace("/threads")
    });
    pswp.listen('beforeChange', function() {
        Cookies.set(cookieName, pswp.getCurrentIndex());
    });
    pswp.init();
}

