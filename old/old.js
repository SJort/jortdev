function setTestCookie(){
    let item = {};
    item.title = "Comic Con";
    item.date = "2020-03-30";

    let item2 = {};
    item2.title = "Photoshoot";
    item2.date = "2020-03-29";

    let item3 = {};
    item3.title = "Vakantie";
    item3.date = "2020-05-29";

    let items = [];
    items.push(item);
    items.push(item2);
    items.push(item3);
    let result = "";
    items.forEach(function(item){
        result += JSON.stringify(item) + ";";
    });
    console.log("Result: " + result);

    Cookies.set("data", result);
    loadTable();
}
