<?php
// Include config file for db
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/config.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/account/login-functions.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/comments/vote-functions.php";

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FORM SUBMIT FUNCTIONS
// Define variables and initialize with empty values
$comment = "";
$comment_error = "";
$comment_id = "";

//this gets triggered when submit is pressed
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!is_logged_in()) {
        $comment_error = "You need to be logged in to post.";
    }
    $comment = $_POST["comment-field"];
    if (strlen($comment) > 255) {
        $comment_error = "Comment too long (" . strlen($comment) . "/255).";
    }

    $comment_id = $_POST["comment_id"];
    if (empty($comment)) {
        $comment_error = "Comment is empty.";
    }
//    if($comment === "error"){
//        $comment_error = "Error test success";
//    }

    if (empty($comment_error)) {
        $user_id = $_SESSION["user_id"];


        $sql = "INSERT INTO comment (user_id, content, parent_comment_id) VALUES (:user_id, :content, :comment_id);";

        global $pdo;
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $stmt->bindParam(":content", $comment, PDO::PARAM_STR);

            //-1 is for parent comment form, so no reply to another comment
            if ($comment_id === "-1") {
                $comment_id = NULL;
            }
            $stmt->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);

            if ($stmt->execute()) {
//                $comment_err = "Comment posted!";
                //redirect to same page to avoid form page resubmission error
                header("Location: " . $_SERVER["PHP_SELF"]);
            } else {
                $comment_err = "Failed to post comment.";
            }
        }
        unset($stmt);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  COMMENT FUNCTIONS


function echo_td()
{
    echo "<td class='comment-table-cell'>";
}

function did_user_vote($comment_id, $type)
{
    session_go();
    if (!is_logged_in()) {
        return false;
    }
    $user_id = $_SESSION["user_id"];
    $sql = "SELECT COUNT(*) AS total FROM comment_link 
        WHERE comment_id=:comment_id AND link_type=:type AND user_id=:user_id;";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $statement->bindParam(":comment_id", $comment_id, PDO::PARAM_STR);
        $statement->bindParam(":type", $type, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                $row = $statement->fetch();
                $total = $row["total"];
                if ($total === "0") {
                    return false;
                }
                return true;
            }
        }
    }
    return false;
}

function echo_votes($comment_id, $up)
{
//    vague idea: adjust background color according to votes

    $up_string = $up ? "up" : "down"; //for the ids and stuff: upvoteButton-1
    $vote_code = $up ? "1" : "2"; //in the query, 1 means upvote and 2 downvote
    $image_src = "/images/" . $up_string . "vote" . (did_user_vote($comment_id, $vote_code) ? "d" : "") . ".svg";

    echo "<input type='image' src='" . $image_src . "' comment=" . $comment_id . " id=" . $up_string . "vote-" . $comment_id . " 
    onclick='vote(this, " . bts($up) . ")' " . " alt='" . $up_string . "vote button'/>";

    echo "(<span comment=" . $comment_id . " id=" . ($up ? "up" : "down") . "votecount-" . $comment_id . ">";
    echo get_vote_count($comment_id, $up);
    echo "</span>)";
}

function echo_date($date)
{
    echo "<span style='font-size: 10px; color:grey'>" . $date . "</span>";
}

function echo_reply_button($pid)
{
    global $comment_id;
    $image_src = "/images/" . ($pid === $comment_id ? "close" : "reply") . ".svg";
    echo "<input type='image' src='" . $image_src . "' comment=" . $pid . " id=reply-" . $pid . " 
    onclick='toggle(this)' " . " alt='Reply button'/>";
}

function echo_reply_form($pid, $username, $show)
{
    global $comment_id, $comment_error, $comment; //these are set in the POST thing

    //local variables, otherwise globals get overwritten at the else
    $comment_text = "";
    $comment_err = "";

    //if the send comment is this comment, show the error and comment text
    if ($comment_id === $pid) {
        $comment_err = $comment_error;
        $comment_text = $comment;
        $show = true;
    }

    echo "<div comment=" . $pid . " id=form-" . $pid . " style='display:" . ($show ? "block" : "none") . "'>";

    echo "<form action='" . htmlspecialchars($_SERVER["PHP_SELF"]) . "' method='post'>";
    echo "<div class='form-group " . (!empty($comment_err) ? "has-error" : "") . "'>";
    echo "<label>";
    echo "<textarea name='comment-field' id=comment_field-" . $pid . " class='input'  rows='4' cols='40'>$comment_text</textarea>";
    echo "</label>";

    echo "<input type='hidden' name='comment_id' value=" . $pid . " />";
    echo "<br>";
    echo "<span class='help-block'>" . $comment_err . "</span>";
    echo "</div>";

    echo "<div class='form-group'>";
    $submit_text = "Submit " . ($username === "-1" ? "comment" : "reply to " . $username);

    echo "<input type='submit' class='input' value='" . $submit_text . "'>";
    echo "</div>";
    echo "</form>";

    echo "</div>";
}

/**
 * @param $pid String The ID of the comment.
 * @return bool True if the comment with the ID has replies.
 */
function has_children($pid)
{
    $sql = "SELECT comment.parent_comment_id FROM comment WHERE parent_comment_id=:pid";
    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":pid", $pid, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                return true;
            }
        }
    }
    return false;
}

/**
 * @param $row array The database result row.
 * @param $is_child_row boolean True if the row to be printed is a child of another row.
 */
function echo_comment_row($row, $is_child_row)
{
    $id = $row["id"];
    $row_start_html = "<tr class='comment-row'>";

    //spacing with above comment
    if (!$is_child_row) {
        echo $row_start_html;
        echo_td();
        echo "<br>";
        echo "</td>";
        echo "</tr>";
    }

    //row with user, upvote, downvote and reply button
    echo $row_start_html;
    echo_td();
    $url = "/user/" . $row["username"];
    echo "<a class='link' href='", $url, "'><i>", $row["username"], "</i></a>";
    echo_spacing();
    echo_votes($id, true);
    echo_spacing();
    echo_votes($id, false);
    echo_spacing();
    echo_reply_button($id);
    echo_spacing();
    echo_date($row["creation_time"]);
    echo "</td>";
    echo "</tr>";

    //row with content
    echo "<tr class='comment-body'>";
    echo_td();
    echo $row["content"];
    echo "</td>";
    echo "</tr>";

    //row with reply form
    echo $row_start_html;
    echo_td();
    echo_reply_form($id, $row["username"], false);
    echo "</tr>";
    echo "</td>";

    //row with comment children
    if (has_children($id)) {
        echo "<tr>";
        echo_td();
        echo_child_comments($id);
        echo "</td>";
        echo "</tr>";
    }
}

/**
 * @param $pid String The ID of the comment to print all the replies of, including all nested replies.
 */
function echo_child_comments($pid)
{
    if (!has_children($pid)) {
        return;
    }

    //merges the ids of comment with the usernames from user
    $sql = "SELECT user.username, comment.content, comment.id, comment.parent_comment_id, DATE_FORMAT(comment.creation_time, '%e/%m/%y: %H:%i') as creation_time
                    FROM comment INNER JOIN user ON user.id=comment.user_id where parent_comment_id=:pid 
                    ORDER BY comment.creation_time DESC;";

    global $pdo;
    if ($statement = $pdo->prepare($sql)) {
        $statement->bindParam(":pid", $pid, PDO::PARAM_STR);
        if ($statement->execute()) {
            if ($statement->rowCount() > 0) {
                while ($row = $statement->fetch()) {
                    echo "<table class='comment-child-table'>";
                    echo_comment_row($row, true);
                    echo "</table>";
                }
            }
        }
    }
}

/**
 * Prints all the comments in a table.
 */
function echo_parent_comments()
{
    //merges the ids of comment with the usernames from user
    $sql = "SELECT user.username, comment.content, comment.id, comment.parent_comment_id, DATE_FORMAT(comment.creation_time, '%e/%m/%y: %H:%i') as creation_time
        FROM comment INNER JOIN user ON user.id=comment.user_id WHERE comment.parent_comment_id IS NULL
        ORDER BY comment.creation_time DESC;";


    global $pdo;
    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                echo "<table class='comment-parent-table'>";
                while ($row = $stmt->fetch()) {
                    echo_comment_row($row, false);
                }
                echo "</table>";
            }
        }
        unset($stmt);
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Comments</title>
    <!--    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">-->
    <script src="/js/base-devel.js"></script>
    <script src="/comments/comments.js"></script>
    <link rel="stylesheet" href="/css/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">
    <style>
        /*COMMENTS//////////////////////////////////////////////////////////////////////////////////////////////////////*/
        /*!*the root table with the parent comments*!*/
        /*.comment-parent-table {*/
        /*    border-bottom: 2px solid black;*/
        /*    margin-bottom: 10px;*/

        /*}*/

        /*!*the table which replies / comments that have a parent_Comment_id*!*/
        /*.comment-child-table {*/
        /*    border-left: 1px solid lightgrey;*/
        /*    padding-left: 20px;*/
        /*    border-collapse: collapse;*/
        /*}*/

        /*!*each <tr> in the comment table*!*/
        /*.comment-row {*/
        /*    white-space: nowrap;*/
        /*}*/

        /*!*each td in the comment table*!*/
        /*.comment-table-cell {*/
        /*    padding-left: 20px;*/
        /*    !*white-space: nowrap;*!*/
        /*}*/

        /*!*the td which contains the comment text*!*/
        /*.comment-body {*/
        /*    word-break: break-word;*/
        /*}*/
    </style>
</head>

<?php require_once $_SERVER["DOCUMENT_ROOT"] . "/account/profile-box.php"; ?>

<body>
<div class="center-horizontal-parent">
    <div class="center-child" style="width:600px">
        <h2>Leave a comment</h2>
        <?php echo_reply_form("-1", "-1", true); ?>

        <div class="center-child">
            <?php
            echo_parent_comments();
            ?>
        </div>
    </div>
</body>
</html>

