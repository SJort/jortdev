<?php
// Include config file for db
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/config.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/account/login-functions.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/account/profile-box.php";

// Define variables and initialize with empty values
$comment = "";
$comment_error = "";

//this gets triggered when submit is pressed
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!is_logged_in()) {
        $comment_error = "You need to be logged in to post.";
    }
    $comment = $_POST["comment"];
    if (strlen($comment) > 255) {
        $comment_error = "Comment too long (" . strlen($comment) . "/255).";
    }

    if (empty($comment)) {
        $comment_error = "Comment is empty.";
    }

    if (empty($comment_error)) {
        $user_id = $_SESSION["user_id"];

        $sql = "INSERT INTO comment (user_id, content) VALUES (:user_id, :content);";

        global $pdo;
        if ($stmt = $pdo->prepare($sql)) {

            $stmt->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $stmt->bindParam(":content", $comment, PDO::PARAM_STR);

            if ($stmt->execute()) {
//                $comment_err = "Comment posted!";
                //redirect to same page to avoid form page resubmission error
                header("Location: " . $_SERVER["PHP_SELF"]);
            }
            else {
                $comment_error = "Failed to post comment.";
            }

        }
        unset($stmt);

    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Comments</title>
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="center-horizontal-parent">
<div class="center-child" style="width:350px">
    <h2>Leave a comment</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($comment_error)) ? 'has-error' : ''; ?>">
            <label>Comment
                <textarea name="comment" class="form-control" value="<?php echo $comment; ?>" rows="5"
                          cols="40"></textarea>
            </label>
            <span class="help-block"><?php echo $comment_error; ?></span>
        </div>
        <p>Posting as <i><?php echo $_SESSION["username"]; ?></i></p>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit">
        </div>
    </form>

    <div class="center-child">
        <table class="table">
            <?php
            //merges the ids of comment with the usernames from user
            $sql = "SELECT user.username, comment.content FROM comment INNER JOIN user ON user.id=comment.user_id ORDER BY comment.creation_time DESC;";

            global $pdo;
            if ($stmt = $pdo->prepare($sql)) {
                if ($stmt->execute()) {
                    if ($stmt->rowCount() > 0) {
                        while ($row = $stmt->fetch()) {
                            $url = "/user/" . $row["username"];
                            echo "<tr><th>", $row["content"], "<br><a class='link' href='", $url, "'> - <i>", $row["username"], "</i></a></th></tr>";
                        }
                    }
                }
                unset($stmt);
            }
            ?>
        </table>
    </div>

    <?php
    ?>

</div>
</body>
</html>
