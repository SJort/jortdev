<?php

$error = "";
function getError(){
    global $error;
    return $error;
}

function setError($errorToSet){
    global $error;
    $error = $errorToSet;
}

function bts($b){
    return $b ? 'true' : 'false';
}
