<?php
// Initialize the session
session_start();

$selected_user = "Anonymous";
if (isset($_GET["user"])) {
    $selected_user = $_GET["user"];
}

$user_exists = false;

require_once "config.php";

$sql = "SELECT * FROM users WHERE username = :username;";

if ($stmt = $pdo->prepare($sql)) {
    $stmt->bindParam(":username", $selected_user, PDO::PARAM_STR);
    if ($stmt->execute()) {
        if ($stmt->rowCount() > 0) {
            $user_exists = true;
        }
    } else {
        error("Failed to execute query.");
    }
    unset($stmt);
}

//// Check if the user is logged in, if not then redirect him to login page
//if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
//    header("location: login.php");
//    exit;
//}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Welcome</title>
    <link rel="stylesheet" href="/libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/stylesheet.css">
</head>
<body class="centered-flex-root-container">
<div class="centered-flex-item">
    <?php
    if (!$user_exists) {
        echo "<h1 class='display-1'>This user does not exist</h1>";
    } else {
        echo "<h1 class='display-1'>$selected_user</h1>";
        if (isset($_SESSION["username"])) {
            echo "Logged in as ", htmlspecialchars($_SESSION["username"]);
        } else {
            echo "Not logged in";
        }
        echo "<br>";
        echo "<br>";
        echo '<a href="/account/reset-password" class="btn btn-warning">Reset Your Password</a><br>';
        echo "<br>";
        echo '<a href="/account/logout" class="btn btn-danger">Sign Out of Your Account</a>';
    }
    ?>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/account/profile-box.php"; ?>
<script src="/libraries/jquery-3.4.1.min.js"></script>
<script src="/libraries/bootstrap/js/bootstrap.bundle.js"></script>
</body>
</html>
